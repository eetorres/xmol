/*
 * cpalette.h
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#include <cpalette.h>

CPalette::CPalette(void)
{
    _ipalette = 0;
}

void CPalette::set(double w)
{
    xdelta1 = w/w0;
    xdelta2 = xdelta1+xdelta1;
    x0=xdelta1;
    x1=xdelta1+x0;
    x2=xdelta1+x1;
    x3=xdelta1+x2;
    x4=xdelta1+x3;
    x5=xdelta1+x4;
    ydelta1 = 255.0;
}

void CPalette::set_color(unsigned int u)
{
    _ipalette = u;
    switch(u)
    {
    case 0:
        w0 = 6.0;//Linear
        break;
    case 3:
        w0 = 6.0;//HSV
        break;
    default:
        w0 = 6.0;//HSV
        break;
    }
};

void CPalette::update_palette_index(void)
{
    _index_palette.clear();
    set(zmax);
    _index_palette.resize(_lvls+1);
#ifdef _PALETTE_SHOW_MESSAGE_
    printf("_lvls = %i\n",_lvls);
    printf("dz = %f\n",dz);
#endif
    id_rgb.r=0;
    id_rgb.g=0;
    id_rgb.b=0;
    for(unsigned int _p=0; _p<(_lvls+1); _p++)
    {
        _index_palette[_p] = (id_rgb/255.0f);
        id_rgb.r++;
        if(id_rgb.r > 255)
        {
            id_rgb.r = 0;
            id_rgb.g++;
            if(id_rgb.g > 255)
            {
                id_rgb.g = 0;
                id_rgb.b++;
            }
        }
        //std::cout<<"ir="<<id_rgb.r<<" ig="<<id_rgb.g<<" ib="<<id_rgb.b<<std::endl;
        //std::cout<<"fr="<<_index_palette[_p].r<<" fg="<<_index_palette[_p].g<<" fb="<<_index_palette[_p].b<<std::endl;
    }
}

glm::vec3 CPalette::get_color(unsigned int c)
{
    //unsigned int c = (unsigned int)(val/dz);
    //unsigned int c = (unsigned int)val;
    return _color_palette[c];
}

glm::vec3 CPalette::get_index(unsigned int c)
{
    return _index_palette[c];
}

unsigned int CPalette::get_index_rgb(int r, int g, int b)
{
    uint index_=0;
    if(b > 0)
    {
        index_=b*256*256;
    }
    if(g > 0)
    {
        index_+=g*256;
    }
    if(r > 0)
    {
        index_+=r;
    }
    return index_;
}

/*
unsigned int CPalette::get_index_rg(glm::tvec3<uint> u){
  uint index_=0;
  if(u.g > 0){
    index_+=u.g*256;
  }
  if(u.r > 0){
    index_+=u.r;
  }
  return index_;
}*/

/*
glm::tvec3<uint> CPalette::get_vcolor(double val){
  unsigned int c = (unsigned int)(val);
  return _color_palette[c];
}*/

/// RUBISH


