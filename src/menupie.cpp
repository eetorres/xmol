
#include <imgui.h>
#undef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include "menupie.h"

#include <imgui_internal.h>

// https://github.com/ocornut/imgui/issues/434
// Return >= 0 on mouse 0 click
// Optional int* p_selected display and update a currently selected item
int PiePopupSelectMenu(const ImVec2& center, const char* popup_id, std::vector<std::string>& items, int items_count, int* p_selected, bool* sw)
{
    int ret = -1;
    // FIXME: Missing a call to query if Popup is open so we can move the PushStyleColor inside the BeginPopupBlock (e.g. IsPopupOpen() in imgui.cpp)
    // FIXME: Our PathFill function only handle convex polygons, so we can't have items spanning an arc too large else inner concave edge artifact is too visible, hence the ImMax(7,items_count)
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0,0,0,0));
    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0,0,0,0));
    ImGui::SetNextWindowPos({-100, -100});
    // 434: Lapinozz commented Oct 8, 2016
    //if(ImGui::BeginPopupModal(popup_id))
    bool res = ImGui::BeginPopup(popup_id);
    if(res)
    {
        const ImVec2 drag_delta = ImVec2(ImGui::GetIO().MousePos.x - center.x, ImGui::GetIO().MousePos.y - center.y);
        const float drag_dist2 = drag_delta.x*drag_delta.x + drag_delta.y*drag_delta.y;

        const ImGuiStyle& style = ImGui::GetStyle();
        const float RADIUS_MIN = 30.0f;
        const float RADIUS_MAX = 120.0f;
        const float RADIUS_INTERACT_MIN = 20.0f;
        const int ITEMS_MIN = 6;

        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        draw_list->PushClipRectFullScreen();
        draw_list->PathArcTo(center, (RADIUS_MIN + RADIUS_MAX)*0.5f, 0.0f, IM_PI*2.0f*0.99f, 32);   // FIXME: 0.99f look like full arc with closed thick stroke has a bug now
        draw_list->PathStroke(ImColor(0,0,0), true, RADIUS_MAX - RADIUS_MIN);

        const float item_arc_span = 2*IM_PI / ImMax(ITEMS_MIN, items_count);
        float drag_angle = atan2f(drag_delta.y, drag_delta.x);
        if (drag_angle < -0.5f*item_arc_span)
            drag_angle += 2.0f*IM_PI;
        //ImGui::Text("%f", drag_angle);    // [Debug]

        int item_hovered = -1;
        // 434: Horrowind commented Dec 14, 2015
        for (int item_n = 0; item_n < items_count; item_n++)
        {
            const char* item_label = (const char*)items[item_n].c_str();
            const float inner_spacing = style.ItemInnerSpacing.x / RADIUS_MIN / 2;
            const float item_inner_ang_min = item_arc_span * (item_n - 0.5f + inner_spacing);
            const float item_inner_ang_max = item_arc_span * (item_n + 0.5f - inner_spacing);
            const float item_outer_ang_min = item_arc_span * (item_n - 0.5f + inner_spacing * (RADIUS_MIN / RADIUS_MAX));
            const float item_outer_ang_max = item_arc_span * (item_n + 0.5f - inner_spacing * (RADIUS_MIN / RADIUS_MAX));

            bool hovered = false;
            if (drag_dist2 >= RADIUS_INTERACT_MIN*RADIUS_INTERACT_MIN)
                //if (drag_dist2 >= RADIUS_INTERACT_MIN*RADIUS_INTERACT_MIN && drag_dist2 < RADIUS_MAX*RADIUS_MAX)
            {
                if (drag_angle >= item_inner_ang_min && drag_angle < item_inner_ang_max)
                    hovered = true;
            }
            //if(drag_dist2 < RADIUS_MAX*RADIUS_MAX){
            bool selected = p_selected && (*p_selected == item_n);
            int arc_segments = (int)(32 * item_arc_span / (2*IM_PI)) + 1;
            draw_list->PathArcTo(center, RADIUS_MAX - style.ItemInnerSpacing.x, item_outer_ang_min, item_outer_ang_max, arc_segments);
            draw_list->PathArcTo(center, RADIUS_MIN + style.ItemInnerSpacing.x, item_inner_ang_max, item_inner_ang_min, arc_segments);
            //draw_list->PathFill(window->Color(hovered ? ImGuiCol_HeaderHovered : ImGuiCol_FrameBg));
            draw_list->PathFillConvex(hovered ? ImColor(100,100,150) : selected ? ImColor(120,120,140) : ImColor(70,70,70));
            ImVec2 text_size = ImGui::GetFont()->CalcTextSizeA(ImGui::GetFontSize(), FLT_MAX, 0.0f, item_label);
            ImVec2 text_pos = ImVec2(
                                  center.x + cosf((item_inner_ang_min + item_inner_ang_max) * 0.5f) * (RADIUS_MIN + RADIUS_MAX) * 0.5f - text_size.x * 0.5f,
                                  center.y + sinf((item_inner_ang_min + item_inner_ang_max) * 0.5f) * (RADIUS_MIN + RADIUS_MAX) * 0.5f - text_size.y * 0.5f);
            draw_list->AddText(text_pos, ImColor(255,255,255), item_label);
            if (hovered)
                item_hovered = item_n;
            //}
        }
        /* 434: Ocornut commented Dec 11, 2015
        for (int item_n = 0; item_n < items_count; item_n++)
        {
            const char* item_label = items[item_n];
            const float item_ang_min = item_arc_span * (item_n+0.02f) - item_arc_span*0.5f; // FIXME: Could calculate padding angle based on how many pixels they'll take
            const float item_ang_max = item_arc_span * (item_n+0.98f) - item_arc_span*0.5f;

            bool hovered = false;
            if (drag_dist2 >= RADIUS_INTERACT_MIN*RADIUS_INTERACT_MIN)
            {
                if (drag_angle >= item_ang_min && drag_angle < item_ang_max)
                    hovered = true;
            }
            bool selected = p_selected && (*p_selected == item_n);

            int arc_segments = (int)(32 * item_arc_span / (2*IM_PI)) + 1;
            draw_list->PathArcTo(center, RADIUS_MAX - style.ItemInnerSpacing.x, item_ang_min, item_ang_max, arc_segments);
            draw_list->PathArcTo(center, RADIUS_MIN + style.ItemInnerSpacing.x, item_ang_max, item_ang_min, arc_segments);
            //draw_list->PathFill(window->Color(hovered ? ImGuiCol_HeaderHovered : ImGuiCol_FrameBg));
            draw_list->PathFillConvex(hovered ? ImColor(100,100,150) : selected ? ImColor(120,120,140) : ImColor(70,70,70));

            ImVec2 text_size = ImGui::GetFont()->CalcTextSizeA(ImGui::GetFontSize(), FLT_MAX, 0.0f, item_label);
            ImVec2 text_pos = ImVec2(
                    center.x + cosf((item_ang_min + item_ang_max) * 0.5f) * (RADIUS_MIN + RADIUS_MAX) * 0.5f - text_size.x * 0.5f,
                    center.y + sinf((item_ang_min + item_ang_max) * 0.5f) * (RADIUS_MIN + RADIUS_MAX) * 0.5f - text_size.y * 0.5f);
            draw_list->AddText(text_pos, ImColor(255,255,255), item_label);

            if (hovered)
                item_hovered = item_n;

        }*/
        draw_list->PopClipRect();
        //if (ImGui::IsMouseReleased(0))
        //ImGui::CloseCurrentPopup();
        if (ImGui::IsMouseClicked(0))
            //if (ImGui::IsMouseReleased(0))
        {
            ImGui::CloseCurrentPopup();
            *sw = false;
            if(drag_dist2 < RADIUS_MAX*RADIUS_MAX)
            {
                ret = item_hovered;
                if (p_selected)
                    *p_selected = item_hovered;
            }
        }
        ImGui::EndPopup();
    }
    ImGui::PopStyleColor(2);
    return ret;
    //return res;
}


