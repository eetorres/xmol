/*
 * CDisplay.cpp
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "cdisplay.h"

#include <shaders/atom.h>
#include <shaders/pick.h>
#include <shaders/bond.h>
#include <shaders/arrow.h>
#include <shaders/font.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/component_wise.hpp>

//#define _ATOM_DEBUG_MESSAGES_
//#define _DEBUG_ATOM_PICKING_

CDisplay::CDisplay(void)
{
    input_format = 0;
    nvtx = 0;
    selected_atom = MAX_ATOM_INDEX;
    atom_scale = 1.0f;
    bond_scale = 1.0f;
    view_zoom = 1.0f;
    font_scale = 0.005f;
    Rot = glm::mat4(1.0f);
    view_offset = glm::vec2(0.0f,0.0f);
    pcb_images = glm::vec3(1.0f,1.0f,1.0f);
    selected_zatom=0;
    selected_symbol="";
    is_visible = false;
    is_picking = false;
    is_show_box = false;
    is_show_bond = false;
    is_show_symbol = true;
    is_show_index = false;
    is_atom_selected = false;
    //
    selected_xyz = glm::vec3(0.0, 0.0, 0.0);
    initial_axis_position = glm::vec3(0.85f,-0.80f,0.50f);
    SetAxesPosition(initial_axis_position);
    // ------------------------------------------------------------------
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // Create sphere vertex and set the buffers
    pick.set_color(0);
    CreateCylinder(20);
    CreateArrow(20);
    CreateSphere(2);
    SetCylinderBuffer();
    SetArrowBuffer();
    SetSphereBuffer();
    // Create and compile our shaders
    AtomShader.SetShader( atom_vs, atom_frag );
    BondShader.SetShader( bond_vs, bond_frag );
    FontShader.SetShader( font_vs, font_frag );
    PickShader.SetShader( pick_vs, pick_frag );
    ArrowShader.SetShader( arrow_vs, arrow_frag );
    // Load defaulf font
    LoadFont();
    // Initialize the tracker
    track.SetRotationMatrix();
};

void CDisplay::ResetScene(void)
{
    ResetAxes();
    ResetZoom();
    SetViewOffset();
}

void CDisplay::ResetAxes(void)
{
    SetAxesPosition(initial_axis_position);
}

void CDisplay::ResetZoom(void)
{
    view_zoom = 1.0f;
}

void CDisplay::ReadFile(std::string d,std::string f)
{
    bool res = ReadStructureFile(d,f,input_format);
    if(res)
    {
        CreateCube();
        pick.initialize(0,GetTotalAtoms(),GetTotalAtoms());
        pick.update_palette_index();
        is_visible = true;
        is_show_box = IsPeriodic();
    }
}

void CDisplay::SaveFile(std::string f)
{
    SaveStructureFile(f,f,input_format);
}

void CDisplay::ShowBonds(bool b)
{
    is_show_bond = b;
}

void CDisplay::ShowSymbols(bool b)
{
    is_show_symbol = b;
}

void CDisplay::ShowIndex(bool b)
{
    is_show_index = b;
}

/*
void CDisplay::CenterCoordinates(void)
{
    CenterCell();
    DeleteCube();
    //glDeleteVertexArrays(1,&VAOC);
    //glDeleteBuffers(1, &VBOC);
    CreateCube();
}*/

// Create cube with unitary side length
void CDisplay::CreateCube(void)
{
    v_supercell.resize(CUBE_SIDES);
    for(uint i = 0; i < CUBE_SIDES; i++)
    {
        v_supercell[i] = m_cell_box*cube_strip2[i];
    }
    glGenVertexArrays(1, &VAOC);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAOC);
    // Object Vertex
    glGenBuffers(1, &VBOC);
    glBindBuffer(GL_ARRAY_BUFFER, VBOC);
    glBufferData(GL_ARRAY_BUFFER, v_supercell.size()*sizeof(glm::vec3), v_supercell.data(), GL_STATIC_DRAW);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), ( GLvoid * ) 0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray(0);  // Unbind VAOC
}

// Cylinder with unitary length
void CDisplay::CreateCylinder(uint res)
{
    glm::vec3 vt;
    float C_2PI = 2.0*glm::pi<float>();
    // check resolution
    if(res < 20)
    {
        cylinder_vertex = 20;
    }
    else
    {
        cylinder_vertex = res;
    }
    v_cylinder.resize(cylinder_vertex);
    const GLdouble step = C_2PI / (double)(cylinder_vertex-2);
    for(int i = 0; i < cylinder_vertex; ++ i)
    {
        vt.x = std::cos(i * step);
        vt.y = std::sin((i * step));
        vt.z = (i % 2 == 0 ? -1.0 : 1.0);
        v_cylinder[i] = vt;
    }
}

// Arrow with unitary length
void CDisplay::CreateArrow(uint res)
{
    glm::vec3 vt;
    float C_2PI = 2.0*glm::pi<float>();
    // check resolution
    if(res < 20)
    {
        arrow_vertex = 20;
    }
    else
    {
        arrow_vertex = res;
    }
    v_arrow.resize(arrow_vertex);
    const GLdouble step = C_2PI / (double)(arrow_vertex-2);
    for(int i = 0; i < arrow_vertex; ++ i)
    {
        vt.x = std::cos(i * step);
        vt.y = std::sin((i * step));
        vt.z = (i % 2 == 0 ? 0.0 : 1.0);
        v_arrow[i] = vt;
    }
    v_cone.resize(arrow_vertex);
    vt.x = 0;
    vt.y = 0;
    vt.z = 1.2;
    v_cone[0] = vt;
    for(int i = 1; i < arrow_vertex; ++ i)
    {
        vt.x = 2.0f*std::cos(i * step);
        vt.y = 2.0f*std::sin((i * step));
        vt.z = 1.0;
        v_cone[i] = vt;
    }
}

// Create sphere vertex with unitary radius
void CDisplay::CreateSphere(uint res)
{
    int cont=0;
    glm::vec3 vt;
    uint s, i, j;
    triangle *tp;
    uint u_sphere_rows = 1 << res;
    uint u_sphere_strip_length=20*(u_sphere_rows*(u_sphere_rows-1)+(u_sphere_rows*3));
    v_sphere.resize(u_sphere_strip_length);
    /* iterate over the 20 sides of the icosahedron */
    for(s = 0; s < 20; s++)
    {
        tp = (triangle *)&icosahedron[s];
        for(i = 0; i < u_sphere_rows; i++)
        {
            // create a tstrip for each row
            // number of triangles in this row is number in previous +2
            // strip the ith trapezoid block
            glm::vec3 v0, v1, v2, v3, va, vb;
            LinearMix(tp->pt[1], tp->pt[0], (float)(i+1)/u_sphere_rows, v0);
            LinearMix(tp->pt[1], tp->pt[0], (float)i/u_sphere_rows, v1);
            LinearMix(tp->pt[1], tp->pt[2], (float)(i+1)/u_sphere_rows, v2);
            LinearMix(tp->pt[1], tp->pt[2], (float)i/u_sphere_rows, v3);
            v_sphere[cont] = glm::normalize(v0);
            cont++;
            v_sphere[cont] = glm::normalize(v1);
            cont++;
            for(j = 0; j < i; j++)
            {
                // calculate 2 more vertices at a time
                LinearMix(v0, v2, (float)(j+1)/(i+1), va);
                LinearMix(v1, v3, (float)(j+1)/i, vb);
                v_sphere[cont] = glm::normalize(va);
                cont++;
                v_sphere[cont] = glm::normalize(vb);
                cont++;
            }
            v_sphere[cont] = glm::normalize(v2);
            cont++;
        }
    }
    nvtx = cont;
#ifdef _ATOM_DEBUG_MESSAGES_
    std::cout<<" FL_GL_ATOM: sphere rows = "<<u_sphere_rows<<std::endl;
    std::cout<<" FL_GL_ATOM: sphere_strip_length = "<<u_sphere_strip_length<<std::endl;
    std::cout<<" Number of vertex = "<<cont<<std::endl;
#endif
};

void CDisplay::SetSphereBuffer(void)
{
    //GLuint normalbuffer;
    glGenVertexArrays(1, &VAOM);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAOM);
    // Object Vertex
    glGenBuffers(1, &VBOM);
    glBindBuffer(GL_ARRAY_BUFFER, VBOM);
    glBufferData(GL_ARRAY_BUFFER, v_sphere.size() * sizeof(glm::vec3), v_sphere.data(), GL_STATIC_DRAW);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), ( GLvoid * ) 0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray( 0 );  // Unbind VAOM
}

void CDisplay::SetCylinderBuffer(void)
{
    //GLuint normalbuffer;
    glGenVertexArrays(1, &VAOB);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAOB);
    // Object Vertex
    glGenBuffers(1, &VBOB);
    glBindBuffer(GL_ARRAY_BUFFER, VBOB);
    glBufferData(GL_ARRAY_BUFFER, v_cylinder.size() * sizeof(glm::vec3), v_cylinder.data(), GL_STATIC_DRAW);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), ( GLvoid * ) 0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray( 0 );  // Unbind VAOB
}

void CDisplay::SetArrowBuffer(void)
{
    glGenBuffers(1, &VBOA);
    glBindBuffer(GL_ARRAY_BUFFER, VBOA);
    // Set vertex array
    glGenVertexArrays(1, &VAOA);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAOA);
    // Object Vertex
    glBufferData(GL_ARRAY_BUFFER, v_arrow.size() * sizeof(glm::vec3), v_arrow.data(), GL_STATIC_DRAW);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), ( GLvoid * ) 0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray( 0 );  // Unbind VAOA
    /// HERE ///
    glGenBuffers(1, &VBOT);
    glBindBuffer(GL_ARRAY_BUFFER, VBOT);
    // Set vertex array
    glGenVertexArrays(1, &VAOT);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAOT);
    // Object Vertex
    //glGenBuffers(1, &VBOA);
    //glBindBuffer(GL_ARRAY_BUFFER, VBOA);
    glBufferData(GL_ARRAY_BUFFER, v_cone.size() * sizeof(glm::vec3), v_cone.data(), GL_STATIC_DRAW);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), ( GLvoid * ) 0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray( 0 );  // Unbind VAOA
}

void CDisplay::DrawScene(int w, int h)
{
    target_width = view_zoom*glm::compMax(v_box_size);
    fix_side = target_width/20;
    //target_height = 3*v_box_size.y;
    track.SetViewSize(w,h);
    //float A = target_width / target_height; // target aspect ratio
    if (w <= h)
    {
        float A = (float)w / (float)h;
        float tw = A*target_width;
        // wide viewport, use full height
        Projection = glm::ortho(A*(-target_width-view_offset.x),A*(target_width-view_offset.x),-target_width-view_offset.y,target_width-view_offset.y,-10*target_width,10*target_width); // In world coordinates
        FixView = glm::ortho(-A*target_width,A*target_width,-target_width,target_width,-10*target_width,10*target_width); // In world coordinates
    }
    else
    {
        float A = (float)h / (float)w;
        // tall viewport, use full width
        Projection = glm::ortho(-target_width-view_offset.x,target_width-view_offset.x,A*(-target_width-view_offset.y),A*(target_width-view_offset.y),-10*target_width,10*target_width); // In world coordinates
        FixView = glm::ortho(-target_width,target_width,-A*target_width,A*target_width,-10*target_width,10*target_width); // In world coordinates
    }
    // Ortho camera matrix
    // PUT ALL THIS MATRICES OUTSIDE THE DRAW SCENE TO AVOID REDUNDENCE
    // ModelViewProjection in the multiplication of our all matrices
    //glm::mat4 Zoom = glm::mat4(1.0f);
    //Zoom = glm::translate( Zoom, glm::vec3( 0.0f, 0.0f, -20.0f ) );
    //mvp = Projection * Zoom * track.Model; // Remember, matrix multiplication is the other way around
    //mvp = Projection * track.Model; // Remember, matrix multiplication is the other way around
    ///////////////////////////////////////////////
    if(is_picking)
    {
#ifdef _DEBUG_ATOM_PICKING_
        DEBUG_MESSAGE("Start picking")
#endif
        PickShader.Use();
        DrawAtoms(PickShader.Program,is_picking);
    }
    // Show atom spheres
    AtomShader.Use();
    if(is_show_box)
        DrawBox(AtomShader.Program);
    DrawAtoms(AtomShader.Program,is_picking);
    // Show bond cilinders
    BondShader.Use();
    DrawBonds(AtomShader.Program);
    // Show atoms symbols/lables
    if(is_show_label)
    {
        FontShader.Use();
        RenderText();
    }
    // Show axes
    ArrowShader.Use();
    ShowAxes(ArrowShader.Program);
}

void CDisplay::DrawBox(GLuint Program)
{
    // Get matrix's uniform location and set matrix
    // Remember, matrix multiplication is the other way around
    mvp = Projection * track.Model;
    GLint ModelViewProjection = glGetUniformLocation( Program, "MVP" );
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    GLint ModelProjection = glGetUniformLocation( Program, "M" );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( track.Model ) );
    GLint ViewProjection = glGetUniformLocation( Program, "V" );
    glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( track.View ) );
    GLint AtomScale = glGetUniformLocation( Program, "S" );
    glUniform1f( AtomScale, 1.0 );
    // Draw the cube
    glBindVertexArray(VAOC);
    GLint cColor = glGetUniformLocation( Program, "C" ); // size
    glm::vec3 col = glm::vec3(1.0f,0.0f,0.0f);
    glUniform3fv( cColor, 1, &col.x );
    glDrawArrays(GL_LINE_STRIP, 0, 16);
}

void CDisplay::DrawAtoms(GLuint Program, bool b)
{
    glm::vec3 acolor;
    glm::mat4 Model2;
    glm::vec3 acenter = glm::vec3(1.0f,0.0f,0.0f);
    glBindVertexArray(VAOM);
    // Draw atoms in the supercell
    //AtomShader.Use();
    GLint ModelViewProjection = glGetUniformLocation( Program, "MVP" );
    GLint ModelProjection = glGetUniformLocation( Program, "M" );
    GLint ViewProjection = glGetUniformLocation( Program, "V" );
    GLint AtomScale = glGetUniformLocation( Program, "S" ); // size
    GLint AtomColor = glGetUniformLocation( Program, "C" ); // size
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( track.Model ) );
    glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( track.View ) );
    //
    // process start picking
    // turn off texturing, lighting and fog during picking
    if(b)
    {
        glClearColor(1,1,1,1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    for( int x=v_pbc_x.x; x<v_pbc_x.y; x++){
    for( int y=v_pbc_y.x; y<v_pbc_y.y; y++){
    for( int z=v_pbc_z.x; z<v_pbc_z.y; z++){
    //glm::vec3 cell_xyz = glm::vec3(x*v_box_size.x,v_box_size.y,v_box_size.z);
    glm::vec3 cell_xyz;
    glm::mat4 trkModel;
    cell_xyz = (float(x)*glm::vec3(m_cell_box[0]));//glm::vec3(x*v_box_size.x,0,0);
    trkModel = glm::translate(track.Model, cell_xyz);
    cell_xyz = (float(y)*glm::vec3(m_cell_box[1]));
    trkModel = glm::translate(trkModel, cell_xyz);
    cell_xyz = (float(z)*glm::vec3(m_cell_box[2]));//glm::vec3(x*v_box_size.x,0,0);
    trkModel = glm::translate(trkModel, cell_xyz);
    glm::mat4 PrjVw = Projection * track.View;
    //DEBUG_MESSAGE("Drawing cell")
    //DEBUG_VEC3("cell_xyz=",cell_xyz);
    for( uint i=0; i<GetTotalAtoms(); i++)
    {
        Model2 = glm::translate( trkModel, v_float_raw_xyz[i]);
        //DEBUG_MESSAGE("Drawing atoms")
        //DEBUG_VEC3("v_float_raw_xyz[i]=",v_float_raw_xyz[i]);
        //mvp = Projection * track.View * Model2;
        mvp = PrjVw * Model2;
        glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
        //glUniform1f( AtomScale, string symbol[xyz.v_atom_index[i]] );
        glUniform1f( AtomScale, atom_scale*atom_rgbs[v_atom_index[i]][3] );
        if(b)
        {
            acolor = pick.get_index(i);
        }
        else if(is_atom_selected && (i == selected_atom))
        {
            //acolor = glm::vec3(1.5*atom_rgbs[v_atom_index[i]][1],1.5*atom_rrgb[v_atom_index[i]][2],1.5*atom_rrgb[v_atom_index[i]][3]);
            //acolor = 1.5f*glm::vec3(atom_rgbs[v_atom_index[i]]);
            acolor = 1.5f*glm::vec3(1.0,0.5,0.3);
//#ifdef _DEBUG_ATOM_PICKING_
//          DEBUG_MESSAGE("Start picking")
//          DEBUG_VEC3("acolor=",acolor);
//#endif
        }
        else
        {
            acolor = glm::vec3(atom_rgbs[v_atom_index[i]]);
        }
        glUniform3fv( AtomColor, 1, &acolor.x );
        glDrawArrays(GL_TRIANGLE_STRIP, 0, nvtx);
    }
    }
    }
    }
    if(b)
    {
        ProcessPicking(true);
        glClearColor(CColor.x,CColor.y,CColor.z,CColor.w);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#ifdef _DEBUG_ATOM_PICKING_
        DEBUG_VEC3("acolor=",acolor)
        DEBUG_VALUE("selected_index=",selected_index)
        DEBUG_VALUE("Total atoms=",GetTotalAtoms())
        DEBUG_MESSAGE("End picking")
#endif
        is_picking = false;
    }
}

void CDisplay::SetPicking(int cx, int cy)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    pcolor[0]=cx;
    pcolor[1]=viewport[3]-cy;
    //std::cout<<" vx = "<<(int)viewport[2]<<" vy = "<<(int)viewport[3]<<std::endl;
    is_picking = true;
}

void CDisplay::ProcessPicking(bool b)
{
    unsigned char pixel[3];
    //uint pixel[3];
    //std::cout<<" px = "<<pcolor[0]<<" py = "<<pcolor[1]<<std::endl;
    glReadPixels(pcolor[0], pcolor[1], 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
    //std::cout<<" x = "<<(int)pixel[0]<<" y = "<<(int)pixel[1]<<" z = "<<(int)pixel[2]<<std::endl;
    selected_atom = pick.get_index_rgb((int)pixel[0],(int)pixel[1],(int)pixel[2]);
    if(selected_atom < MAX_ATOM_INDEX)
    {
        is_atom_selected = true;
        //std::cout<<" atom = "<<selected_index<<std::endl;
        //std::cout<<" number = "<<v_atom_index[selected_index]<<std::endl;
        //std::cout<<" symbol = "<<symbol[v_atom_index[selected_index]]<<std::endl;
        selected_zatom=v_atom_index[selected_atom]+1;
        selected_symbol=symbol[v_atom_index[selected_atom]];
        selected_xyz=v_float_raw_xyz[selected_atom];
    }
    else
    {
        is_atom_selected = false;
    }
}

void CDisplay::DrawBonds(GLuint Program)
{
    glm::vec3 acolor;
    glm::mat4 Model1;
    glm::mat4 Model2;
    if(is_bond_eval && is_show_bond)
    {
        //std::cout<<"b_show_bonds && is_bond_visible "<<b_show_bonds<<" "<<is_bond_visible<<std::endl;
        glBindVertexArray(0);
        glBindVertexArray(VAOB);
        GLint ModelViewProjection = glGetUniformLocation( Program, "MVP" );
        GLint ModelProjection = glGetUniformLocation( Program, "M" );
        GLint ViewProjection = glGetUniformLocation( Program, "V" );
        GLint BondScale = glGetUniformLocation( Program, "S" ); // size
        GLint AtomColor = glGetUniformLocation( Program, "C" ); // color
        glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( track.View ) );
        acolor = glm::vec3(0.9f,0.9f,0.9f);
        //glUniform3fv(CenterPosition, 1, glm::value_ptr(acenter));
        for( int i=0; i<i_number_of_bonds; i++)
        {
            glm::mat4 Rot = glm::mat4(1.0f);
            Rot = glm::rotate(Rot, m_bond_ang[i].x, glm::vec3(0, 0, 1));
            Rot = glm::rotate(Rot, m_bond_ang[i].y, glm::vec3(0, 1, 0));
            Model1 = track.Model * Rot;
            //if(is_centered)
            Model2 = glm::translate( track.Model, v_bond_position_xyz[i]) * Rot;
            //else
            //Model2 = glm::translate( track.Model, m_bond_xyz[i] ) * Rot;
            //Model2 = glm::translate( track.Model, m_bond_xyz[i] ) * Rot;
            mvp = Projection * track.View * Model2;
            glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
            glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model1 ) );
            //glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( track.Model ) );
            glm::vec3 bscale = glm::vec3(0.1,0.1,0.5*v_bond_length[i]);
            //glUniform3fv( BondScale, 1, &bscale.x ); // bond radius
            glUniform3fv(BondScale, 1, &bscale.x);
            glUniform3fv(AtomColor, 1, &acolor.x);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, cylinder_vertex);
        }
    }
}

void CDisplay::ShowAxes(GLuint Program)
{
    glm::vec3 acolor;
    //glm::vec3 vaxes;
    glm::mat4 Model1 = glm::mat4(1.0f);
    glm::mat4 Model2;
    //
    glBindVertexArray(0);
    glBindVertexArray(VAOA);
    GLint ModelViewProjection = glGetUniformLocation( Program, "MVP" );
    GLint ModelProjection = glGetUniformLocation( Program, "M" );
    GLint ViewProjection = glGetUniformLocation( Program, "V" );
    GLint AxesPosition = glGetUniformLocation( Program, "P" ); // Position
    GLint BondScale = glGetUniformLocation( Program, "S" ); // size
    GLint AtomColor = glGetUniformLocation( Program, "C" ); // color
    //
    glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( track.View ) );
    //glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( Model1 ) );
    glm::vec3 bscale = glm::vec3(0.1*fix_side,0.1*fix_side,2.0*fix_side);
    glUniform3fv(BondScale, 1, &bscale.x);
    //
    glUniform3fv(AxesPosition, 1, &axis_position.x);
    // X
    acolor = glm::vec3(0.9f,0.0f,0.0f);
    glm::mat4 Rot = glm::mat4(1.0f);
    Rot = glm::rotate(Rot, 1.57079633f, glm::vec3(0, 1, 0));
    //Model1 = track.Model * Rot;
    Model2 = track.Model * Rot;
    //mvp = Projection * track.View * Model2;
    mvp = FixView * track.View * Model2;
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model2 ) );
    glUniform3fv(AtomColor, 1, &acolor.x);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, arrow_vertex);
    // Y
    acolor = glm::vec3(0.0f,0.9f,0.0f);
    Rot = glm::mat4(1.0f);
    Rot = glm::rotate(Rot, -1.57079633f, glm::vec3(1, 0, 0));
    Model2 = track.Model * Rot;
    mvp = FixView * track.View * Model2;
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model2 ) );
    glUniform3fv(AtomColor, 1, &acolor.x);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, arrow_vertex);
    // Z
    acolor = glm::vec3(0.0f,0.0f,0.9f);
    Rot = glm::mat4(1.0f);
    Rot = glm::rotate(Rot, 1.57079633f, glm::vec3(0, 0, 1));
    Model2 = track.Model * Rot;
    mvp = FixView * track.View * Model2;
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model2 ) );
    glUniform3fv(AtomColor, 1, &acolor.x);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, arrow_vertex);
    glBindVertexArray(0);
    ///////////////////////////////////////////////////////////////////////////////
    /// HERE ///
    glBindVertexArray(VAOT);
    //GLint ModelViewProjection = glGetUniformLocation( Program, "MVP" );
    //GLint ModelProjection = glGetUniformLocation( Program, "M" );
    //GLint ViewProjection = glGetUniformLocation( Program, "V" );
    //GLint BondScale = glGetUniformLocation( Program, "S" ); // size
    //GLint AtomColor = glGetUniformLocation( Program, "C" ); // color
    //
    glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( track.View ) );
    //glUniformMatrix4fv( ViewProjection, 1, GL_FALSE, glm::value_ptr( Model1 ) );
    //glm::vec3 bscale = glm::vec3(0.1*fix_side,0.1*fix_side,2.0*fix_side);
    glUniform3fv(BondScale, 1.0f, &bscale.x);
    //vaxes = glm::vec3(4, 4, 4);
    // X
    acolor = glm::vec3(0.9f,0.0f,0.0f);
    Rot = glm::mat4(1.0f);
    Rot = glm::rotate(Rot, 1.57079633f, glm::vec3(0, 1, 0));
    //Model1 = track.Model * Rot;
    Model2 = track.Model * Rot;
    mvp = FixView * track.View * Model2;
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model2 ) );
    glUniform3fv(AtomColor, 1, &acolor.x);
    glDrawArrays(GL_TRIANGLE_FAN, 0, arrow_vertex);
    // Y
    acolor = glm::vec3(0.0f,0.9f,0.0f);
    Rot = glm::mat4(1.0f);
    Rot = glm::rotate(Rot, -1.57079633f, glm::vec3(1, 0, 0));
    Model2 = track.Model * Rot;
    mvp = FixView * track.View * Model2;
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model2 ) );
    glUniform3fv(AtomColor, 1, &acolor.x);
    glDrawArrays(GL_TRIANGLE_FAN, 0, arrow_vertex);
    // Z
    acolor = glm::vec3(0.0f,0.0f,0.9f);
    Rot = glm::mat4(1.0f);
    Rot = glm::rotate(Rot, 1.57079633f, glm::vec3(0, 0, 1));
    Model2 = track.Model * Rot;
    mvp = FixView * track.View * Model2;
    glUniformMatrix4fv( ModelViewProjection, 1, GL_FALSE, glm::value_ptr( mvp ) );
    glUniformMatrix4fv( ModelProjection, 1, GL_FALSE, glm::value_ptr( Model2 ) );
    glUniform3fv(AtomColor, 1, &acolor.x);
    glDrawArrays(GL_TRIANGLE_FAN, 0, arrow_vertex);
    glBindVertexArray(0);
}

void CDisplay::DeleteAtom(uint u)
{
    DeleteAtomIndex(u);
    pick.initialize(0,GetTotalAtoms(),GetTotalAtoms());
    pick.update_palette_index();
    selected_atom = MAX_ATOM_INDEX;
    is_atom_selected = false;
}

/* Deprecated
void CDisplay::SetAtomCoordinates(uint u,glm::vec3& v3)
{
    v_float_raw_xyz[u]=v3;
}*/

void CDisplay::UpdateAtomType(uint u)
{
    SetElement(selected_atom,u);
    selected_zatom=v_atom_index[selected_atom]+1;
    selected_symbol=symbol[v_atom_index[selected_atom]];
    //is_atom_selected = false;
}

void CDisplay::SetAtomScale(bool b)
{
    if(b)
    {
        atom_scale = 0.6f;
    }
    else
    {
        atom_scale = 1.0f;
    }
    // atom_scale = f;
}

void CDisplay::SetBondScale(float f)
{
    bond_scale = f;
}

void CDisplay::SetZoom(float f)
{
    view_zoom += f;
    if(view_zoom < 0.1)
        view_zoom = 0.1;
}

void CDisplay::SetFont(float f)
{
    font_scale = f;
}

void CDisplay::RenderText(void)
{
    glm::mat4 Model2;
    std::string str;
    GLfloat ascale;
    for( uint i=0; i<GetTotalAtoms(); i++)
    {
        Model2 = glm::mat4(0.0f);
        Model2 = glm::translate( track.Model, v_float_raw_xyz[i] );
        //Model2 = glm::translate( track.Model, v_atom_raw_xyz[i] );
        mvp = Projection * track.View * Model2;
        if(is_show_symbol && is_show_index)
            str = symbol[v_atom_index[i]] + std::to_string(i);
        else if(is_show_symbol)
            str = symbol[v_atom_index[i]];
        else
            str = std::to_string(i);
        ascale = atom_scale*atom_rgbs[v_atom_index[i]][3];
        RenderText(FontShader.Program, ascale,str);
    }
}

void CDisplay::RenderText(GLuint Program, float s, std::string text)
{
    GLfloat x = -0.15f;
    GLfloat y = -0.15f;
    //GLfloat font_scale = 0.008f;
    glm::mat4 Model2;
    // Activate corresponding render state
    //shader.use();
    glm::vec3 color = glm::vec3(1.0f,1.0f,0.0f);
    mvp =  mvp*glm::inverse(track.Model);
    //glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(target_width), 0.0f, static_cast<GLfloat>(target_height));
    GLint ModelViewProjection = glGetUniformLocation(Program, "MVP");
    glUniformMatrix4fv(ModelViewProjection, 1, GL_FALSE, glm::value_ptr(mvp));
    glUniform3fv(glGetUniformLocation(Program, "textColor"), 1, &color.x);
    glUniform1f(glGetUniformLocation(Program, "S"), s);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(VAOF);
    // Iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = Characters[*c];
        GLfloat xpos = x + ch.Bearing.x * font_scale;
        GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * font_scale;
        GLfloat w = ch.Size.x * font_scale;
        GLfloat h = ch.Size.y * font_scale;
        // Update VBO for each character
        GLfloat vertices[6][4] =
        {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, VBOF);
        // Be sure to use glBufferSubData and not glBufferData
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels
        // by 64 to get amount of pixels))
        x += (ch.Advance >> 6) * font_scale;
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void CDisplay::LoadFont(void)
{
    // FreeType
    FT_Library ft;
    // All functions return a value different than 0 whenever an error occurred
    if (FT_Init_FreeType(&ft))
        std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
    // Load font as face
    FT_Face face;
#if defined(_WIN32)
    if (!FT_New_Face(ft, "C:\\Windows\\Fonts\\Arial.ttf", 0, &face))
        std::cout << "MESSAGE::FREETYPE: loaded Arial font" << std::endl;
    else if (!FT_New_Face(ft, "C:\\Windows\\Fonts\\Courier-New.ttf", 0, &face))
        std::cout << "MESSAGE::FREETYPE: loaded Win font" << std::endl;
    else if (!FT_New_Face(ft, "C:\\Windows\\Fonts\\Calibri.ttf", 0, &face))
        std::cout << "MESSAGE::FREETYPE: loaded Calibri font" << std::endl;
    else
    {
        std::cout << "ERROR::FREETYPE: Failed to load Win font" << std::endl;
    }
#else
    if (!FT_New_Face(ft, "/usr/share/fonts/truetype/roboto/hinted/Roboto-Medium.ttf", 0, &face))
        std::cout << "MESSAGE::FREETYPE: loaded font /usr/share/fonts/truetype/roboto/hinted/Roboto-Medium.ttf" << std::endl;
    else if (!FT_New_Face(ft, "/usr/share/fonts/truetype/liberation2/LiberationMono-Regular.ttf", 0, &face))
        std::cout << "MESSAGE::FREETYPE: loaded font /usr/share/fonts/truetype/liberation2/LiberationMono-Regular.ttf" << std::endl;
    else if (!FT_New_Face(ft, "/usr/share/fonts/truetype/liberation/LiberationMono-Regular.ttf", 0, &face))
        std::cout << "MESSAGE::FREETYPE: loaded font /usr/share/fonts/truetype/liberation/LiberationMono-Regular.ttf" << std::endl;
    else
    {
        std::cout << "ERROR::FREETYPE: Failed to load the follwing fonts" << std::endl;
        std::cout << "/usr/share/fonts/truetype/roboto/hinted/Roboto-Medium.ttf" << std::endl;
        std::cout << "/usr/share/fonts/truetype/liberation2/LiberationMono-Regular.ttf" << std::endl;
        std::cout << "/usr/share/fonts/truetype/liberation/LiberationMono-Regular.ttf" << std::endl;
    }
#endif
    // Set size to load glyphs as
    FT_Set_Pixel_Sizes(face, 0, 48);
    // Disable byte-alignment restriction
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    // Load first 128 characters of ASCII set
    for (GLubyte c = 0; c < 128; c++)
    {
        // Load character glyph
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }
        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer
        );
        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Now store character for later use
        Character character =
        {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            (GLuint)face->glyph->advance.x
        };
        Characters.insert(std::pair<GLchar, Character>(c, character));
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    // Destroy FreeType once we're finished
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
    // Configure VAO/VBO for texture quads
    glGenVertexArrays(1, &VAOF);
    glGenBuffers(1, &VBOF);
    glBindVertexArray(VAOF);
    glBindBuffer(GL_ARRAY_BUFFER, VBOF);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void CDisplay::SetTrackLastPosition(float x, float y)
{
    track.SetLastVector(x,y);
}

void CDisplay::SetTrackCurretPosition(float x, float y)
{
    track.SetTrackMotion(x,y);
}

void CDisplay::SetTrackRotation(float x, float y)
{
    track.SetInitialViewX(x,y);
}

void CDisplay::SetTrackLastRotation(void)
{
    track.SetLastRotation();
}

// linear interpolation between a & b, by fraction f
inline void CDisplay::LinearMix(glm::vec3 &a, glm::vec3 &b, GLfloat f, glm::vec3 &r)
{
    r=a+f*(b-a);
}

void CDisplay::DeleteCube(void)
{
    glDeleteVertexArrays(1,&VAOC);
    glDeleteBuffers(1, &VBOC);
}

void CDisplay::DeleteCylinder(void)
{
    glDeleteVertexArrays(1,&VAOB);
    glDeleteBuffers(1, &VBOB);
}

void CDisplay::DeleteArrow(void)
{
    glDeleteVertexArrays(1,&VAOA);
    glDeleteBuffers(1, &VBOA);
}

void CDisplay::DeleteSphere(void)
{
    glDeleteVertexArrays(1,&VAOA);
    glDeleteBuffers(1, &VBOA);
}

void CDisplay::DeleteAllResources(void)
{
    // Cleanup: deallocate all resources
    DeleteCube();
    DeleteCylinder();
    DeleteArrow();
    DeleteSphere();
    glDeleteVertexArrays(1,&VAOT);
    glDeleteVertexArrays(1,&VAOF);
    glDeleteBuffers(1, &VAOT);
    glDeleteBuffers(1, &VBOF);
}

// END SPHERE
