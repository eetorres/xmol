/*
 * Main.cpp
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

// ImGui - standalone example application for GLFW + OpenGL 3, using programmable pipeline
// If you are new to ImGui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)
// (GL3W is a helper library to access OpenGL functions since there is no standard header to access modern OpenGL functions easily. Alternatives are GLEW, Glad, etc.)

#include "imgui.h"
#include "config.h"
#include <imgui_internal.h>

#ifdef _WIN32
#define WINVER 0x0600
#define _WIN32_WINNT 0x0600
#include <windef.h> // On Windows we have MAX_PATH too
#include <initguid.h>
#include <KnownFolders.h>
#include <shlobj.h>
char config[MAX_PATH];
bool DoesFileExist(LPCSTR pszFilename)
{
   DWORD dwAttrib = GetFileAttributes(pszFilename);
   if ( ! (dwAttrib & FILE_ATTRIBUTE_DEVICE) &&
        ! (dwAttrib & FILE_ATTRIBUTE_DIRECTORY))
   {
      return true;
   }
   return false;
}
#endif //_WIN32

//#include "imgui_impl_glfw_gl3.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <xmol_debug.h>
#include "imguifilesystem.h"
#include "menupie.h"
#include "cdisplay.h"
#include "version.h"
#include "cinit.h"

//#define _XMOL_MAIN_DEBUG_
#define _SHOW_PIE_MENUE_
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 640

static char dirPath[ImGuiFs::MAX_PATH_BYTES];
static char filePath[ImGuiFs::MAX_PATH_BYTES];


static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Error %d: %s\n", error, description);
}

std::string sections(CInit &reader)
{
    std::stringstream ss;
    std::set<std::string> sections = reader.Sections();
    for (std::set<std::string>::iterator it = sections.begin(); it != sections.end(); ++it)
        ss << *it << ",";
    return ss.str();
}

std::string definit = "[format] version=1 [settings] bgr = 0.03 bgg = 0.12 bgb = 0.33\n";

int main(int, char**)
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    // settings
    //glfwWindowHint( GLFW_SAMPLES, 4 ); // slow down the redering
    glfwWindowHint( GLFW_DOUBLEBUFFER, GLFW_TRUE );
    // GLFW_RED_BITS, GLFW_GREEN_BITS, GLFW_BLUE_BITS, GLFW_ALPHA_BITS, GLFW_DEPTH_BITS
    glfwWindowHint( GLFW_RED_BITS, 8 );
    glfwWindowHint( GLFW_GREEN_BITS, 8 );
    glfwWindowHint( GLFW_BLUE_BITS, 8 );
    glfwWindowHint( GLFW_ALPHA_BITS, 8 );
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    // GLFW_REFRESH_RATE, GLFW_DONT_CARE = highest
    glfwWindowHint( GLFW_REFRESH_RATE, GLFW_DONT_CARE );
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Xmol ImGui GLFW+OpenGL3.3", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    glfwInit();
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return -1;
    }
    //XkbFreeKeyboard(desc, 0, True);
    // Setup Dear ImGui binding
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    io.ConfigWindowsMoveFromTitleBarOnly = true;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls
    //ImGui_ImplGlfwGL3_Init(window, true);
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    // Setup style
    ImGui::StyleColorsDark();
    //
#ifdef BUILD_FOR_LINUX
    const char * p_user;
    char config[200];
    p_user=getenv("USER");
    sprintf(config,"/home/%s/.xmol-org/xmol.ini",p_user);
    std::ifstream infile(config);
    if(infile.is_open())
    {
        std::cout<<" settings: "<<config<<std::endl;
        infile.close();
    }
    else
    {
        int ans=0;
        char cmd[1024];
        sprintf(cmd,"mkdir -p /home/%s/.xmol-org/",p_user);
        std::cout<<" settings: "<<cmd<<std::endl;
        ans = system(cmd);
    }
    io.IniFilename = (const char*)config;
#else
    LPWSTR wszPath = NULL;
    // Local AppData - Vista and greater
    if(SUCCEEDED(SHGetKnownFolderPath ( FOLDERID_LocalAppData, KF_FLAG_CREATE, NULL, &wszPath )))
    {
        printf("\nSHGetKnownFolderPath FOLDERID_LocalAppData = %S\n", wszPath);
        sprintf(config,"%S\\xmol-org\\xmol.ini",wszPath);
        std::string valid_ini = config;
        std::replace(valid_ini.begin(), valid_ini.end(), '\\', '/');

        /*for(int i=0; i<valid_ini.length(); i++){
                if(valid_ini[i]==' ')
                    valid_ini[i]='\x20';
                  //valid_ini.insert(i++,"\\");

        }*/
        strcpy(config,valid_ini.c_str());
        io.IniFilename = (const char*)config;
        //std::ifstream infile(config);
        std::cout<<" valid_ini: "<<valid_ini<<std::endl;
        std::cout<<" config: "<<config<<std::endl;
        std::cout<<" io.IniFilename: "<<io.IniFilename<<std::endl;
        //if(infile.is_open())
        if(DoesFileExist(config))
        {
            std::cout<<" settings: "<<config<<std::endl;
            //infile.close();
        }
        else
        {
            int ans=0;
            char cmd[1024];
            sprintf(cmd,"No found: %S\\xmol-org",wszPath);
            std::cout<<" Message: "<<cmd<<std::endl;
            sprintf(cmd,"mkdir %S\\xmol-org",wszPath);
            ans = system(cmd);
        }

    }
    // Per user Documents - Vista and greater
    //if(SUCCEEDED(SHGetKnownFolderPath ( FOLDERID_Documents, KF_FLAG_CREATE, NULL, &wszPath )))
    //{
    //    printf("SHGetKnownFolderPath FOLDERID_Documents        =%S\n", wszPath);
    //}
#endif
    //ImGui::StyleColorsClassic();
    //ImGui::StyleColorsLight();
    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'misc/fonts/README.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("./fonts/Karla-Regular.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("./fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("./fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("./fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("./fonts/ProggyTiny.ttf", 10.0f);
    //io.Fonts->AddFontFromFileTTF("./fonts/ProggyClean.ttf", 16.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(io.font != NULL);
    char win_title[256];
    std::string str1, str2, str3;
    int major, minor, rev;
    static char user_version[64];
    static char dev_version[64];
    static char imgui_version[64];
    static char glm_version[64];
    static char glfw_version[64];
    sprintf(user_version,"%s %s(%s)",AutoVersion::STATUS,AutoVersion::UBUNTU_VERSION_STYLE,AutoVersion::DATE);
    sprintf(dev_version,"%s", AutoVersion::FULLVERSION_STRING);
    sprintf(imgui_version,"%s", IMGUI_VERSION);
    sprintf(glm_version,"%i.%i.%i.%i", GLM_VERSION_MAJOR,GLM_VERSION_MINOR,GLM_VERSION_PATCH,GLM_VERSION_REVISION);
    glfwGetVersion(&major, &minor, &rev);
    sprintf(glfw_version,"%i.%i.%i",major,minor,rev);
    bool show_settings_window = true;
    bool show_file_chooser = true;
    bool show_activity_window = true;
    bool show_about_xmol = false;
    ImVec4 clear_color = ImVec4(0.07f, 0.34f, 0.47f, 1.00f);
    CDisplay *view = new CDisplay();
    ///////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef BUILD_FOR_LINUX
    // beta
    char inidata[200];
    sprintf(inidata,"/home/%s/.xmol-org/xmoldata.ini",p_user);
    CInit inicfg(inidata);
    if (inicfg.ParseError() < 0)
    {
        std::cout << "Can't load init"<<inidata<<std::endl;
    }
    else
    {
        std::cout << "Init found "<<inidata<<std::endl;
        clear_color.x = inicfg.GetFloat("settings", "bgr", 0.07);
        clear_color.y = inicfg.GetFloat("settings", "bgg", 0.34);
        clear_color.z = inicfg.GetFloat("settings", "bgb", 0.47);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
#endif
    // ------------------------------------------------------------------
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    view->SetClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    int display_w, display_h;
    float iposx=0, iposy=0;
    float eposx=0, eposy=0;
    float pposx=0, pposy=0;
    bool is_mouse = false;
    bool is_select_pie = false;
    bool is_show_pie = false;
    bool is_atom_lock = true;
    ImVec2 pie_menu_center;
    char buff[5];
    // Main loop
    static int ix = 1, iy = 1, iz = 1;
    static float atom_scale = 1.0f;
    static float bond_scale = 1.0f;
    static float zoom = 0.0f;
    static float font = view->GetFontSize();
    //
    const float f32_zero = 0.0f, f32_one = 0.1f, f32_lo_a = -10000000000.0f, f32_hi_a = +10000000000.0f;
    static bool inputs_step = true;
    // Pie Menu
    static const char* test_data = "Menu";
    const char* format_items[] = { "XYZ", "VASP", "PDB" };
    //std::vector<std::string> items = { "Close", "Move", "Edit", "Select", "Delete", "-" };
    std::vector<std::string> items = { "Symbol", "Index", "Bonds", "Edit", "Delete", "-" };
    int items_count = 6;
    int key_pressed = 0;
    static int selected = -1;
    glm::vec3 axis_position = -view->axis_position;
    //std::string sText;
    //ImGuiStyle& style = ImGui::GetStyle();
    //style.Alpha = 0.5;
    while (!glfwWindowShouldClose(window))
    {
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        //ImGui_ImplGlfwGL3_NewFrame();
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets automatically appears in a window called "Debug".
        {
            //if ( ImGui::IsMousePosValid() && !ImGui::IsAnyItemActive() && !ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem) && !(ImGui::IsMouseHoveringAnyWindow() || ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem)) ){
            //if( !ImGui::IsWindowHovered() && ImGui::IsMouseClicked(1) ){
            if (!is_select_pie && ImGui::IsMousePosValid() &&  (is_mouse || (!ImGui::IsAnyItemActive() && !ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem) && !(ImGui::IsAnyWindowHovered() || ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem)))) )
            {
                if( !ImGui::IsWindowHovered() && ImGui::IsMouseClicked(1) )
                {
                    pposx = io.MousePos.x;
                    pposy = io.MousePos.y;
                    if(view->IsVisible())
                        view->SetPicking(pposx,pposy);
#ifdef _SHOW_PIE_MENUE_
                    is_select_pie=true;
#endif
                }
                else if (ImGui::IsMouseClicked(0))
                {
                    iposx = io.MousePos.x;
                    iposy = io.MousePos.y;
                    view->SetTrackLastPosition(iposx,iposy);
                    if(view->IsVisible())
                    {
                        view->SetPicking(iposx,iposy);
                        is_atom_lock = true;
                    }
                    //#ifdef _SHOW_PIE_MENUE_
                    // is_select_pie=true;
                    //#endif
                }
                else if (ImGui::IsMouseDown(0) && ImGui::IsMouseDragging(0))
                {
                    eposx = io.MousePos.x;
                    eposy = io.MousePos.y;
                    view->SetTrackCurretPosition(eposx,eposy);
                    is_mouse = true;
                }
                else if (ImGui::IsMouseReleased(0) && is_mouse)
                {
                    view->SetTrackLastRotation();
                    is_mouse = false;
                }
                else if((io.MouseWheel != 0.0f))
                {
                    zoom = 0.1*io.MouseWheel;
                    view->SetZoom(zoom);
                }
            }
            else
            {
                //ImGui::Text("Mouse pos: <INVALID>");
                is_mouse = false;
            }

            key_pressed= 0;
            for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++)
            {
                if (ImGui::IsKeyPressed(i))
                {
                    key_pressed = i;
                    //ImGui::SameLine();
                    //ImGui::Text("%d (0x%X)", i, i);
                    break;
                }
            }

            //if(ImGui::IsKeyPressed(261) && view->is_atom_selected) // d
            if((key_pressed == 261) && (view->is_atom_selected)) // d
            {
                //std::cout<<" delete "<<view->selected_atom<<std::endl;
                view->DeleteAtom(view->selected_atom);
            }
            else if(io.KeyCtrl)
            {
                switch(key_pressed)
                {
                case 83: // s
                    show_settings_window = !show_settings_window;
                    break;
                case 65: // a
                    show_activity_window = !show_activity_window;
                    break;
                case 70: // f
                    show_file_chooser = !show_file_chooser;
                    break;
                case 72: // h
                    show_about_xmol = !show_about_xmol;
                    break;
                case 88: // x
                    view->SetTrackRotation(glm::half_pi<float>(),glm::half_pi<float>());
                    break;
                case 89: // y
                    view->SetTrackRotation(glm::half_pi<float>(),0.0);
                    break;
                case 90: // z
                    view->SetTrackRotation(0.0,glm::half_pi<float>());
                    break;
                }
            }
            else
            {
                switch(key_pressed)
                {
                case 83: // s
                    view->is_show_symbol = !view->is_show_symbol;
                    break;
                case 66: // b
                    view->is_show_bond = !view->is_show_bond;
                    view->SetAtomScale(view->is_show_bond);
                    break;
                case 67: // c
                    view->is_show_box = !view->is_show_box;
                    break;
                case 73: // i
                    view->is_show_index = !view->is_show_index;
                    break;
                case 256: // ESC
                    view->ResetScene();
                    axis_position = -view->axis_position;
                    break;
                case 262: // right
                    if(!ImGui::IsAnyItemActive())
                      view->view_offset.x+=1.0f;
                    break;
                case 263: // left
                    if(!ImGui::IsAnyItemActive())
                      view->view_offset.x-=1.0f;
                    break;
                case 265: // down
                    if(!ImGui::IsAnyItemActive())
                      view->view_offset.y+=1.0f;
                    break;
                case 264: // up
                    if(!ImGui::IsAnyItemActive())
                      view->view_offset.y-=1.0f;
                    break;
                default:
                    break;
                }
            }
#ifdef _SHOW_PIE_MENUE_
            if(is_select_pie)
            {
                if(view->is_atom_selected)
                {
                    snprintf(buff, 5, "%s-%u",view->selected_symbol.c_str(),view->selected_atom);
                    items[5]=buff;
                }
                else
                {
                    items[5] = "-";
                }
                ImGui::OpenPopup("##piepopup");
                ImVec2 pie_menu_center = ImVec2(pposx,pposy);
                int n = PiePopupSelectMenu(pie_menu_center, "##piepopup", items, items_count, &selected, &is_select_pie);
                if ( (n >= 0) && view->IsVisible())
                {
                    //if((n == 4) &&
                    switch(n)
                    {
                    case 0: // Symbol
                        view->is_show_symbol = !view->is_show_symbol;
                        break;
                    case 1: // Index
                        view->is_show_index = !view->is_show_index;
                        break;
                    case 2: // Bonds
                        view->is_show_bond = !view->is_show_bond;
                        view->SetAtomScale(view->is_show_bond);
                        break;
                    case 3: // Edit
                        is_atom_lock = false;
                        break;
                    case 4: // Delete
                        if(view->is_atom_selected)
                        {
                            // delete selected atom by the index
                            view->DeleteAtom(view->selected_atom);
                            n = 0; // reset pie menu
                        }
                        break;
                        //is_select_pie = false;
                    }
                }
                //printf("returned %d\n", n);
            }
#endif
            ////////////////////////////////////////////////////////////////////////////////
            // Show file chooser
            ////////////////////////////////////////////////////////////////////////////////
            // In most cases you will use an explicit Begin/End pair to name your windows.
            if (show_file_chooser) // ctr + f
            {
                static ImGuiFs::Dialog dlg;
                static bool disabled = true;                                                   // one per dialog (and must be static)
                //ImGui::SetNextWindowPos(ImVec2(20, 10));
                ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_FirstUseEver);
                ImGui::SetNextWindowSize(ImVec2(300, 130), ImGuiCond_FirstUseEver);
                ImGui::Begin("Choose File (ctrl+f)", &show_file_chooser);
                //ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.06f, 0.06f, 0.06f, 0.94f));
                // Choose file format
                static ImGuiComboFlags flags = 0;
                // General BeginCombo() API, you have full control over your selection data and display type.
                // (your selection data could be an index, a pointer to the object, an id for the object, a flag stored in the object itself, etc.)
                static const char* item_current = format_items[view->input_format];            // Here our selection is a single pointer stored outside the object.
                if (ImGui::BeginCombo("Input Format", item_current, flags)) // The second parameter is the label previewed before opening the combo.
                {
                    for (int n = 0; n < IM_ARRAYSIZE(format_items); n++)
                    {
                        bool is_selected = (item_current == format_items[n]);
                        if (ImGui::Selectable(format_items[n], is_selected))
                            view->input_format = n;
                        item_current = format_items[view->input_format];
                        if (is_selected)
                            ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
                    }
                    ImGui::EndCombo();
                }
                //////
                const bool browseButtonPressed = ImGui::Button("Open");                          // we need a trigger boolean variable
                //if(browseButtonPressed)
                const char* chosenPath = dlg.chooseFileDialog(browseButtonPressed,filePath);             // see other dialog types and the full list of arguments for advanced usage
                if (strlen(chosenPath)>0)
                {
                    // A path (chosenPath) has been chosen RIGHT NOW.
                    // However we can retrieve it later more comfortably using: dlg.getChosenPath()
                    //const char* dirPath = dlg.getLastDirectory();
                    //sprintf(win_title,"Xmol %s",dirPath);
                    strcpy(filePath,dlg.getChosenPath());
                    strcpy(dirPath,dlg.getLastDirectory());
                    view->ReadFile(dirPath,filePath);
                    sprintf(win_title,"Xmol %s",filePath);
                    glfwSetWindowTitle(window, win_title);
                }
                ImGui::SameLine();
                if (ImGui::Button("Reload") && view->IsVisible())
                {
                    view->ReadFile(dirPath,filePath);
                }
                ImGui::SameLine();
                bool saved = view->IsSaved();
                if (saved)
                {
                    ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                    ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
                }
                if (ImGui::Button("Save") && view->IsVisible())
                {
                    view->SaveFile(filePath);
                }
                if(saved)
                {
                    ImGui::PopItemFlag();
                    ImGui::PopStyleVar();
                }

                ImGui::Text("File: %s",dlg.getChosenPath());
                if (ImGui::Button("Close"))
                    show_file_chooser = false;
                ImGui::End();
            }
            ////////////////////////////////////////////////////////////////////////////////
            // Show settings
            ////////////////////////////////////////////////////////////////////////////////
            if (show_settings_window) // ctr + s
            {
                //ImGui::SetNextWindowPos(ImVec2(20, 140));
                ImGui::SetNextWindowPos(ImVec2(0, 455), ImGuiCond_FirstUseEver);
                ImGui::SetNextWindowSize(ImVec2(300, 265), ImGuiCond_FirstUseEver);
                ImGui::Begin("Settings (ctrl+s)", &show_settings_window);
                //ImGui::Text("IsItemFocused() = %d\n", ImGui::IsItemHovered());
                //ImGui::Text("%s",user_version);                             // Display the build version
                //if(ImGui::SliderFloat("radius", &view->atom_scale, 0.0f, 2.0f));//{      // Edit 1 float using a slider from 0.0f to 1.0f
                ImGui::SliderFloat("radius", &view->atom_scale, 0.0f, 2.0f);
                ImGui::SliderFloat("zoom", &view->view_zoom, 0.5f, 10.0f);
                //if(ImGui::SliderFloat("zoom", &view->view_zoom, 0.5f, 10.0f))       // Edit 1 float using a slider from 0.0f to 1.0f
                //{
                //    view->SetZoom(zoom);
                //}
                if(ImGui::SliderFloat("font", &font, 0.001f, 0.01f))       // Edit 1 float using a slider from 0.0f to 1.0f
                {
                    view->SetFont(font);
                }
                // Edit 3 floats representing the background color
                if(ImGui::ColorEdit3("Backgroud", (float*)&clear_color))
                {
                    view->SetClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
#ifdef BUILD_FOR_LINUX
                    // beta
                    inicfg.SetFloat("settings", "bgr", clear_color.x);
                    inicfg.SetFloat("settings", "bgg", clear_color.y);
                    inicfg.SetFloat("settings", "bgb", clear_color.z);
#endif
                }
                // Axes position
                if(ImGui::SliderFloat3("Axes", &axis_position.x, -1.0f, 1.0f))
                {
                    view->SetAxesPosition(axis_position);
                }
                ImGui::SameLine();
                if (ImGui::Button("Reset"))
                {
                    view->ResetScene();
                }
                // View
                ImGui::Text("View");
                ImGui::SameLine();
                if (ImGui::Button("YZ"))
                    view->SetTrackRotation(glm::half_pi<float>(),glm::half_pi<float>());
                ImGui::SameLine();
                if (ImGui::Button("ZX"))
                    view->SetTrackRotation(glm::half_pi<float>(),0.0);
                ImGui::SameLine();
                if (ImGui::Button("XY"))
                    view->SetTrackRotation(0.0,glm::half_pi<float>());
                if(ImGui::Checkbox("Cell", &view->is_show_box)); // Edit bools storing our windows open/close state
                ImGui::SameLine();
                if(ImGui::Checkbox("Bonds", &view->is_show_bond))           // Edit bools storing our windows open/close state
                {
                    view->SetAtomScale(view->is_show_bond);
                }
                ImGui::SameLine();
                if(ImGui::Checkbox("Symbol", &view->is_show_symbol)); // Edit bools storing our windows open/close state
                ImGui::SameLine();
                if(ImGui::Checkbox("Index", &view->is_show_index));   // Edit bools storing our windows open/close state
                view->is_show_label = (view->is_show_index || view->is_show_symbol);
                //ImGui::Text("Cell center");
                //ImGui::InputFloat3("Set", (float*)&view->center_xyz);
                //ImGui::SameLine();
                //if (ImGui::Button("(0,0,0)") && view->IsVisible())
                //    view->CenterCoordinates();
                //static bool disabled = false;
                
                if (ImGui::Button("Close"))
                {
                    show_settings_window = false;
                }
                ImGui::End();
            }
            ////////////////////////////////////////////////////////////////////////////////
            // Show activity
            ////////////////////////////////////////////////////////////////////////////////
            if (show_activity_window) // ctr + a
            {
                ImGui::SetNextWindowPos(ImVec2(1080,0), ImGuiCond_FirstUseEver);
                ImGui::SetNextWindowSize(ImVec2(200, 570), ImGuiCond_FirstUseEver);
                ImGui::Begin("Activity (ctrl+a)", &show_activity_window);
                //ImGui::Text("IsItemFocused() = %d\n", ImGui::IsItemHovered());
                if(view->IsVisible()){
                  ImGui::Text("Cell vectors");
                  ImGui::PushItemWidth(150);
                  //PushStyleCol(..); Text(); PopStyleCol().
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[0].x);
                  ImGui::SameLine();
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[0].y);
                  ImGui::SameLine();
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[0].z);
                  //
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[1].x);
                  ImGui::SameLine();
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[1].y);
                  ImGui::SameLine();
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[1].z);
                  //
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[2].x);
                  ImGui::SameLine();
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[2].y);
                  ImGui::SameLine();
                  ImGui::TextColored(ImVec4(1.0f,1.0f,0.0f,1.0f), "%.3f",view->m_cell_box[2].z);
                  /*
                  ImGui::InputDouble("v11", &view->m_cell_box[0].x, NULL, 1.0f, "%.12f");
                  ImGui::InputDouble("v12", &view->m_cell_box[0].y, NULL, 1.0f, "%.12f");
                  ImGui::InputDouble("v13", &view->m_cell_box[0].z, NULL, 1.0f, "%.12f");
                  //
                  ImGui::InputDouble("v21", &view->m_cell_box[1].x, NULL, 1.0f, "%.12f");
                  ImGui::InputDouble("v22", &view->m_cell_box[1].y, NULL, 1.0f, "%.12f");
                  ImGui::InputDouble("v23", &view->m_cell_box[1].z, NULL, 1.0f, "%.12f");
                  //
                  ImGui::InputDouble("v31", &view->m_cell_box[2].x, NULL, 1.0f, "%.12f");
                  ImGui::InputDouble("v32", &view->m_cell_box[2].y, NULL, 1.0f, "%.12f");
                  ImGui::InputDouble("v33", &view->m_cell_box[2].z, NULL, 1.0f, "%.12f");
                  */
                  ImGui::PopItemWidth();
                  ImGui::Text("Periodic images");
                  ImGui::PushItemWidth(100);
                  if(ImGui::InputInt("x", &ix)){
                     if(ix < 1) ix = 1;
                     view->SetPbcImages(ix,iy,iz);
                     view->UpdateView();
                  }
                  if(ImGui::InputInt("y", &iy)){
                     if(iy < 1) iy = 1;
                     view->SetPbcImages(ix,iy,iz);
                     view->UpdateView();
                  }
                  if(ImGui::InputInt("z", &iz)){
                     if(iz < 1) iz = 1;
                     view->SetPbcImages(ix,iy,iz);
                     view->UpdateView();
                  }
                  ImGui::PopItemWidth();

                if(ImGui::Checkbox("Center structure", &view->is_centered)){
                  view->UpdateView();
                }
                ImGui::Text("Total Atoms: %u",view->GetTotalAtoms());
                for(uint i=0; i<view->v_table_atoms.size(); i++)
                {
                    ImGui::Text(" %3s",symbol[view->GetAtomIndex(i)].c_str());
                    ImGui::SameLine();
                    ImGui::Text(": %u",view->GetAtomTotal(i));
                }
                    ImGui::Text("Selected atom : No. %u",view->selected_atom);
                    ImGui::Text("Symbol [%s], Z [%u]",view->selected_symbol.c_str(),view->selected_zatom);
                    //is_atom_edit
                    if (is_atom_lock)
                    {
                        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
                    }
                    if (ImGui::Button("Symbol:"))
                        ImGui::OpenPopup("select");
                    ImGui::SameLine();
                    ImGui::TextUnformatted(view->selected_zatom == -1 ? "<None>" : view->selected_symbol.c_str());
                    if (ImGui::BeginPopup("select"))
                    {
                        ImGui::Text("Select atom");
                        ImGui::Separator();
                        for (int i = 0; i < PTABLE_ATOMS; i++)
                        {
                            //ImGui::SetCursorPos(ImVec2(10, 10));
                            //ImGui::SetCursorPos(10*(ImVec2&)ptable_position[i].x);
                            ImGui::SetCursorPos(ImVec2(ptable_position[i].x, ptable_position[i].y));
                            if (ImGui::Button(symbol[i].c_str(),ImVec2(30,20)))
                            {
                                view->UpdateAtomType(i);
                                ImGui::CloseCurrentPopup();
                            }
                            if ((i % 10) < 9) ImGui::SameLine();
                        }
                        ImGui::EndPopup();
                    }
                    ImGui::PushItemWidth(150);
                    if(ImGui::InputFloat3("XYZ", &view->selected_xyz.x) \
                            || ImGui::InputScalar("X", ImGuiDataType_Float, &view->selected_xyz.x, inputs_step ? &f32_one : NULL) \
                            || ImGui::InputScalar("Y", ImGuiDataType_Float, &view->selected_xyz.y, inputs_step ? &f32_one : NULL) \
                            || ImGui::InputScalar("Z", ImGuiDataType_Float, &view->selected_xyz.z, inputs_step ? &f32_one : NULL))
                    {
                        //ImGui::Text("%f,%f,%f",view->selected_xyz.x,view->selected_xyz.y,view->selected_xyz.z);
                        view->SetNewCoordinates(view->selected_atom,view->selected_xyz);
                    }
                    ImGui::PopItemWidth();
                    if (is_atom_lock)
                    {
                        ImGui::PopItemFlag();
                        ImGui::PopStyleVar();
                    }
                    if (ImGui::Button("Edit"))
                        is_atom_lock = false;
                }
                ImGui::End();
            }
            ////////////////////////////////////////////////////////////////////////////////
            // Show about Xmol
            ////////////////////////////////////////////////////////////////////////////////
            if (show_about_xmol) // ctr + h
            {
                ImGui::SetNextWindowSize(ImVec2(300, 180), ImGuiCond_FirstUseEver);
                ImGui::Begin("About XMol (ctrl+h)", &show_about_xmol);
                ImGui::Text(" Xmol %s",user_version); // Display the user version
                ImGui::Text(" Release %s",dev_version); // Display the develper version
                ImGui::Text(" Website www.xmol.org"); // Display the develper version
                ImGui::Text(" OpenGL3.3, GLSL %s",glsl_version); // Display the develper version
                ImGui::Text(" ImGUI %s, ",imgui_version); // Display the develper version
                ImGui::Text(" GLFW %s, ",glfw_version); // Display the develper version
                ImGui::Text(" GLM %s, ",glm_version); // Display the develper version
                ImGui::Text(" Contact: support@xmol.org"); // Display the develper version
                ImGui::End();
            }
#ifdef _XMOL_MAIN_DEBUG_
            //ImGui::SetNextWindowPos(ImVec2(20, 270));
            ImGui::Begin("Debug Window");
            ImGui::Text("%s",dev_version);                                // Display the developer version
            if ( ImGui::IsMousePosValid())
            {
                ImGui::Text("Mouse pos: (%g, %g)", io.MousePos.x, io.MousePos.y);
            }
            else
            {
                ImGui::Text("Mouse pos: <INVALID>");
            }
            ImGui::Text("Mouse ipos: (%g, %g)", iposx, iposy);
            ImGui::Text("Mouse epos: (%g, %g)", eposx, eposy);
            ImGui::Text("Mouse down:");
            for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++)
            {
                if (io.MouseDownDuration[i] >= 0.0f)
                {
                    ImGui::SameLine();
                    ImGui::Text("b%d (%.02f secs)", i, io.MouseDownDuration[i]);
                }
            }
            ImGui::Text("Keys down:");
            for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++) if (io.KeysDownDuration[i] >= 0.0f)
                {
                    ImGui::SameLine();
                    ImGui::Text("%d (%.02f secs)", i, io.KeysDownDuration[i]);
                }
            ImGui::Text("Keys pressed:");
            for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++) if (ImGui::IsKeyPressed(i))
                {
                    ImGui::SameLine();
                    ImGui::Text("%d", i);
                }
            //ImGui::Text("Keys release:");   for (int i = 0; i < IM_ARRAYSIZE(io.KeysDown); i++) if (ImGui::IsKeyReleased(i))            { ImGui::SameLine(); ImGui::Text("%d", i); }
            //ImGui::Text("Keys mods: %s%s%s%s", io.KeyCtrl ? "CTRL " : "", io.KeyShift ? "SHIFT " : "", io.KeyAlt ? "ALT " : "", io.KeySuper ? "SUPER " : "");
            //ImGui::Text("NavInputs down:"); for (int i = 0; i < IM_ARRAYSIZE(io.NavInputs); i++) if (io.NavInputs[i] > 0.0f)                    { ImGui::SameLine(); ImGui::Text("[%d] %.2f", i, io.NavInputs[i]); }
            //ImGui::Text("NavInputs pressed:"); for (int i = 0; i < IM_ARRAYSIZE(io.NavInputs); i++) if (io.NavInputsDownDuration[i] == 0.0f)    { ImGui::SameLine(); ImGui::Text("[%d]", i); }
            //ImGui::Text("NavInputs duration:"); for (int i = 0; i < IM_ARRAYSIZE(io.NavInputs); i++) if (io.NavInputsDownDuration[i] >= 0.0f)   { ImGui::SameLine(); ImGui::Text("[%d] %.2f", i, io.NavInputsDownDuration[i]); }
            ImGui::BulletText(
                "IsWindowFocused() = %d\n"
                "IsWindowFocused(_ChildWindows) = %d\n"
                "IsWindowFocused(_ChildWindows|_RootWindow) = %d\n"
                "IsWindowFocused(_RootWindow) = %d\n"
                "IsWindowFocused(_AnyWindow) = %d\n",
                ImGui::IsWindowFocused(),
                ImGui::IsWindowFocused(ImGuiFocusedFlags_ChildWindows),
                ImGui::IsWindowFocused(ImGuiFocusedFlags_ChildWindows | ImGuiFocusedFlags_RootWindow),
                ImGui::IsWindowFocused(ImGuiFocusedFlags_RootWindow),
                ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow));

            // Testing IsWindowHovered() function with its various flags. Note that the flags can be combined.
            ImGui::BulletText(
                "IsWindowHovered() = %d\n"
                "IsWindowHovered(_AllowWhenBlockedByPopup) = %d\n"
                "IsWindowHovered(_AllowWhenBlockedByActiveItem) = %d\n"
                "IsWindowHovered(_ChildWindows) = %d\n"
                "IsWindowHovered(_ChildWindows|_RootWindow) = %d\n"
                "IsWindowHovered(_RootWindow) = %d\n"
                "IsWindowHovered(_AnyWindow) = %d\n",
                ImGui::IsWindowHovered(),
                ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByPopup),
                ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem),
                ImGui::IsWindowHovered(ImGuiHoveredFlags_ChildWindows),
                ImGui::IsWindowHovered(ImGuiHoveredFlags_ChildWindows | ImGuiHoveredFlags_RootWindow),
                ImGui::IsWindowHovered(ImGuiHoveredFlags_RootWindow),
                ImGui::IsWindowHovered(ImGuiHoveredFlags_AnyWindow));
            ImGui::Text("Mouse wheel: %.1f", io.MouseWheel);
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            ImGui::End();
#endif
        }
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClearDepth(1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        if(view->IsVisible())
        {
            // Rendering
            glPushMatrix();
            glLoadIdentity();
            glMatrixMode(GL_MODELVIEW );
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glDepthFunc(GL_LESS);
            glDisable(GL_CULL_FACE);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_BLEND);
            //glDepthFunc(GL_LEQUAL);
            //glCullFace(GL_BACK);
            //glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
            // Draw molecular scene
            view->DrawScene(display_w, display_h);
            //glBindVertexArray(0); // no need to unbind it every time
            //view->RenderText();
            // Now unbind
            //glBindVertexArray(0);
            //glClear(GL_DEPTH_BUFFER_BIT);
            glPopMatrix();
        }
        glUseProgram(0);
        ImGui::Render();
        //ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
    }
    /////////////////////////////////////////////////////////////////////////////
#ifdef BUILD_FOR_LINUX
    // beta
    inicfg.SaveInit();
#endif
    /////////////////////////////////////////////////////////////////////////////
    // Cleanup: Properly deallocate all resources
    view->DeleteAllResources();
    //ImGui_ImplGlfwGL3_Shutdown();
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
