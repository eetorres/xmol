/*
 * CMouseh.cpp
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#include <iostream>
#include <ctrack.h>

// Include GLM
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>

//#define GLM_ENABLE_EXPERIMENTAL
//#include <glm/gtx/string_cast.hpp>
//#include <glm/gtx/norm.hpp>

CTrack::CTrack()
{
    SetInitialView();
    // Camera matrix
    View = glm::lookAt(
               glm::vec3(0,0,4), // Camera is at (4,3,3), in World Space
               glm::vec3(0,0,0), // and looks at the origin
               glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
           );
}

void CTrack::SetInitialView(void)
{
    // modelview matrix (Model) defines how your objects are transformed
    // (translation, rotation and scaling) in your world
    angle = 0.0;
    Rot = glm::mat4(1.0f);
    last_rotation = glm::mat4(1.0f);
    last_rotation = glm::rotate(last_rotation, -1.4f, glm::vec3(1, 0, 0));
    last_rotation = glm::rotate(last_rotation, -1.8f, glm::vec3(0, 0, 1));
    Model = glm::mat4(1.0f);
    rot_matrix = glm::mat4(1.0f);
    axis = glm::vec3(1.0f);
    last_position = glm::vec3(1.0f,0.0f,0.0f);
    current_position = glm::vec3(0.0f);
}

void CTrack::SetInitialViewX(float x, float y)
{
    // modelview matrix (Model) defines how your objects are transformed
    // (translation, rotation and scaling) in your world
    angle = 0.0;
    Rot = glm::mat4(1.0f);
    last_rotation = glm::mat4(1.0f);
    last_rotation = glm::rotate(last_rotation, x, glm::vec3(1, 0, 0));
    last_rotation = glm::rotate(last_rotation, y, glm::vec3(0, 0, 1));
    Model = glm::mat4(1.0f);
    rot_matrix = glm::mat4(1.0f);
    axis = glm::vec3(1.0f);
    last_position = glm::vec3(1.0f,0.0f,0.0f);
    current_position = glm::vec3(0.0f);
    SetRotationMatrix();
}

void CTrack::SetInitialViewZ(void)
{
    // modelview matrix (Model) defines how your objects are transformed
    // (translation, rotation and scaling) in your world
    angle = 0.0;
    Rot = glm::mat4(1.0f);
    last_rotation = glm::mat4(1.0f);
    //last_rotation = glm::rotate(last_rotation, 0f, glm::vec3(1, 0, 0));
    //last_rotation = glm::rotate(last_rotation, 0f, glm::vec3(0, 0, 1));
    Model = glm::mat4(1.0f);
    rot_matrix = glm::mat4(1.0f);
    axis = glm::vec3(1.0f);
    last_position = glm::vec3(1.0f,0.0f,0.0f);
    current_position = glm::vec3(0.0f);
}

void CTrack::SetViewSize(int w, int h)
{
    width  = (float)w;
    height = (float)h;
}

void CTrack::SetTrackMotion(float x, float y)
{
    glm::vec3 dr;
    PointToVector(x, y, current_position);
    // calculate the angle to rotate proportional
    // to the length of the mouse movement
    dr = current_position-last_position;
    angle = glm::l2Norm(dr); // Radians
    // calculate the axis of rotation (cross product)
    axis = glm::cross(last_position,current_position);
    // set for next time
    SetRotationMatrix();
}

void CTrack::SetRotationMatrix(void)
{
    glm::mat4 R = glm::mat4(1.0f);
    Model = glm::mat4(1.0f);
    R = glm::rotate(R, angle, axis);
    rot_matrix = R;
    Model = Model*rot_matrix*last_rotation;
}

void CTrack::SetLastRotation(void)
{
    glm::mat4 tmp = rot_matrix*last_rotation;
    last_rotation = tmp;
}

void CTrack::SetLastVector(float x, float y)
{
    PointToVector(x, y, last_position);
}

void CTrack::PointToVector(float x, float y, glm::vec3& v)
{
    float d, a;
    // project x, y onto a hemi-sphere centered within width, height
    v.x = (2.0*x - width) / width;
    v.y = (height - 2.0*y) / height;
    d = sqrt(v.x*v.x + v.y*v.y);
    //v.z = cos((3.14159265 / 2.0)*((d < 1.0) ? d : 1.0));
    v.z = cos((glm::pi<float>() / 2.0)*((d < 1.0) ? d : 1.0));
    a = 1.0 / glm::l2Norm(v);
    v *= a;
}

// END
