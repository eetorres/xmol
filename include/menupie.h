/*
 * menupie.h
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#ifndef _MENUPIE_H_
#define _MENUPIE_H_

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>


/* Declaration */
int PiePopupSelectMenu(const ImVec2& center, const char* popup_id, std::vector<std::string>&  items, int items_count, int* p_selected, bool* sw);


/* Example */
/*
std::string sText;

if( ImGui::IsWindowHovered() && ImGui::IsMouseClicked( 1 ) )
{
    ImGui::OpenPopup( "PieMenu" );
}

if( BeginPiePopup( "PieMenu", 1 ) )
{
    if( PieMenuItem( "Test1" ) ) sText = "Test1";
    if( PieMenuItem( "Test2" ) )
    {
        sText = "Test2";
        new MyImwWindow3();
    }
    if( PieMenuItem( "Test3", false ) ) sText = "Test3";
    if( BeginPieMenu( "Sub" ) )
    {
        if( BeginPieMenu( "Sub sub\nmenu" ) )
        {
            if( PieMenuItem( "SubSub" ) ) sText = "SubSub";
            if( PieMenuItem( "SubSub2" ) ) sText = "SubSub2";
            EndPieMenu();
        }
        if( PieMenuItem( "TestSub" ) ) sText = "TestSub";
        if( PieMenuItem( "TestSub2" ) ) sText = "TestSub2";
        EndPieMenu();
    }
    if( BeginPieMenu( "Sub2" ) )
    {
        if( PieMenuItem( "TestSub" ) ) sText = "TestSub";
        if( BeginPieMenu( "Sub sub\nmenu" ) )
        {
            if( PieMenuItem( "SubSub" ) ) sText = "SubSub";
            if( PieMenuItem( "SubSub2" ) ) sText = "SubSub2";
            EndPieMenu();
        }
        if( PieMenuItem( "TestSub2" ) ) sText = "TestSub2";
        EndPieMenu();
    }

    EndPiePopup();
}
*/

#endif
