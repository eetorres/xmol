/*
 * mouseh
 *
 * Copyright 2018 Edmanuel <etorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#ifndef _XMOL_DEBUG_H_
#define _XMOL_DEBUG_H_

// DEFINITIONS FOR DEBUGGING

//////////////////////////////////
// XMol Main
//#define _XMOL_MAIN_DEBUG_


////////////////////////////////////////////////////
// Old defines
//////////////////////////////////
// cfragmol.cxx
//////////////////////////////////
//#define _FRAGMOL_DATA_MESSAGES_


/////////////////////////////////
// csupercell.cxx
/////////////////////////////////
//#define _SUPERCELL_DEBUG_BONDS_
//#define _FRAGMENT_DEBUG_MESSAGES_
//#define _FRAGMENT_DEBUG_NEW_FRAGMENT_
//#define _FRAGMENT_DEBUG_DATA_
//#define _FRAGMENT_DEBUG_MOVE_
//#define _FRAGMENT_DEBUG_INTEGRITY_
//#define _FRAGMENT_DEBUG_MATRICES_


//////////////////////////////////
// cfile.cxx
//////////////////////////////////
//#define _FILE_FRAGMOL_DEBUG_MESSAGES_
//#define _FILE_DEBUGING_MESSAGES_
//#define _FILE_DEBUGING_FRAGMENTS_

//////////////////////////////////
// fl_gl_mol_view.cxx
/////////////////////////////////
//#define _GLMOL_DEBUG_MESSAGES_
//#define _GLMOL_DEBUG_PICKING_
//#define _GLMOL_DATA_MESSAGES_
//#define _GLMOL_DEBUG_BOND_COLORS_

//#define _SHOW_DEBUG_CYLINDERS_
//#define _SHOW_DEBUG_LINKED_
//#define _SHOW_DEBUG_BONDS_
//#define _SHOW_DEBUG_PICKING_
//#define _SHOW_INFO_
//#define _SHOW_DEBUG_MESSAGES_
//#define _SHOW_DATA_MESSAGES_
//#define _SHOW_KEY_MESSAGES_
//#define _SHOW_DEBUG_PERIODIC_BONDS_
//#define _SHOW_DEBUG_BOND_COLORS_


/////////////////////////////////
// fl_gl_atom.cxx
/////////////////////////////////
//#define _ATOM_DEBUG_MESSAGES_
//#define _ATOM_DEBUG_BONDS_
//#define _ATOM_FRAGMENT_MESSAGES_


//////////////////////////////////
// cgau.cxx
//////////////////////////////////
//#define _GAU_DEBUG_MESSAGES_
//#define _GAU_INFO_MESSAGES_
//#define _GAU_DEBUG_DATA_


//////////////////////////////////
// CDlp.cxx
//////////////////////////////////
//#define _DLP_INFO_MESSAGES_
//#define _DLP_DEBUG_MESSAGES_
//#define _DLP_COMPOSITON_MESSAGES_
//#define _DLP_DEBUG_DATA_


//////////////////////////////////
// cpdb.cxx
//////////////////////////////////
//#define _PDB_INFO_MESSAGES_
//#define _PDB_DEBUG_MESSAGES_
//#define _PDB_DEBUG_DATA_
//#define _PDB_SHOW_DATA_


//////////////////////////////////
// cposcar.cxx
//////////////////////////////////
//#define _POSCAR_INFO_MESSAGES_
//#define _POSCAR_DEBUG_MESSAGES_
//#define _POSCAR_SHOW_DATA_


//////////////////////////////////
// cpotcar.cxx
//////////////////////////////////
//#define _POTCAR_DEBUG_MESSAGES_


//////////////////////////////////
// cxyz.cxx
//////////////////////////////////
#define _XYZ_INFO_MESSAGES_
#define _XYZ_DEBUG_MESSAGES_
//#define _XYZ_DEBUG_DATA_
//#define _XYZ_DEBUG_COMPOSITION_


//////////////////////////////////
// czmat.cxx
//////////////////////////////////
//#define _ZMAT_INFO_MESSAGES_
//#define _ZMAT_DEBUG_MESSAGES_
//#define _ZMAT_SHOW_DATA_


//////////////////////////////////
// cviewmol.cxx
//////////////////////////////////
//#define _VIEWMOL_MESSAGES_


//////////////////////////////////
// cmapmol.cxx
//////////////////////////////////
//#define _ZMAT_DEBUG_MESSAGES_

//////////////////////////////////
// ctopmol.cxx
//////////////////////////////////
//#define _TOPMOL_DEBUG_MESSAGES_


//////////////////////////////////
// cpalette.cxx
//////////////////////////////////
//#define _PALETTE_SHOW_MESSAGE_

#endif

// END
