#ifndef _PICK_SHADER_H_
#define _PICK_SHADER_H_

const char * pick_frag = "#version 330 core\n"
                         "in vec3 ourColor;\n"
                         "out vec4 outColor;\n"
                         "void main()\n"
                         "{\n"
                         "outColor = vec4(ourColor, 1.0);\n"
                         "}\n\0";

const char * pick_vs = "#version 330 core\n"
                       "layout (location = 0) in vec3 position;\n"
                       "out vec3 ourColor;\n"
                       "uniform mat4 MVP;\n"
                       "uniform mat4 M;\n"
                       "uniform mat4 V;\n"
                       "uniform float S;\n"
                       "uniform vec3 C;\n"
                       "void main()\n"
                       "{\n"
                       "vec3 LightPosition_worldspace = vec3(4,4,4);\n"
                       "gl_Position = MVP * vec4(S*position, 1.0f);\n"
                       "vec3 vertexPosition_cameraspace = ( V * M * vec4(position,1.0f)).xyz;\n"
                       "vec3 LightPosition_cameraspace = ( V * vec4(LightPosition_worldspace,1.0f)).xyz;\n"
                       "ourColor = C;\n"
                       "}\n\0";

#endif
