#ifndef _FONT_SHADER_H_
#define _FONT_SHADER_H_

const char * font_frag = "#version 330 core\n"
                         "in vec2 TexCoords;\n"
                         "out vec4 color;\n"
                         "uniform sampler2D text;\n"
                         "uniform vec3 textColor;\n"
                         "void main()\n"
                         "{\n"
                         "vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);\n"
                         "color = vec4(textColor, 1.0) * sampled;\n"
                         "}\n\0";

const char * font_vs = "#version 330 core\n"
                       "layout (location = 0) in vec4 vertex;\n"
                       "out vec2 TexCoords;\n"
                       "uniform mat4 MVP;\n"
                       "uniform float S;\n"
                       "void main()\n"
                       "{\n"
                       "gl_Position = MVP * vec4(vertex.xy, S, 1.0);\n"
                       "TexCoords = vertex.zw;\n"
                       "}\n\0";

#endif
