/*
 * CTrack.h
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#ifndef _CMOUSE_H_
#define _CMOUSE_H_

#include <time.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/norm.hpp>

#if defined(_WIN32)
typedef unsigned int uint;
#endif // defined

class CTrack
{

public:

    CTrack();
    ~CTrack() {};
    //
    void SetViewSize(int width, int height);
    void SetTrackMotion(float x, float y);
    void PointToVector(float x, float y, glm::vec3& v);
    void SetLastVector(float x, float y);
    void SetRotationMatrix(void);
    void SetLastRotation(void);
    void SetInitialView(void);
    void SetInitialViewX(float x, float y);
    void SetInitialViewZ(void);

//protected:
    // Configure Trackball
    bool  tracking;
    bool  animate;
    float width;
    float height;
    uint  lasttime;
    int   button;
    float angle;
    // Model View
    glm::mat4 Projection;
    glm::mat4 Model;
    glm::mat4 Rot;
    glm::mat4 View;
    glm::mat4 mvp;
    // Trackball Tranfomations
    glm::vec3 axis;
    glm::vec3 current_position;
    glm::vec3 last_position;
    glm::mat4 transform;
    glm::mat4 rot_matrix;
    glm::mat4 last_rotation;
};

#endif
