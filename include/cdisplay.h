/*
 * Sphere.h
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <stdio.h>
#include <iostream>
#include <vector>
#include <map>

// glad to access OpenGL functions
// May use glew/glad/glLoadGen
#include "glad.h"
#include "GLFW/glfw3.h"
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H
// XMol

#include "cshader.h"

#include "cxyz.h"
#include "cmodel.h"
#include "ctrack.h"
#include "cpalette.h"

typedef struct
{
    glm::vec3 pt[3];
} triangle;

#define MAX_ATOM_INDEX 16777215

// six equidistant points lying on the unit sphere //
#define UNIT_SPHERE_XPLUS {  1,  0,  0 }    //  X //
#define UNIT_SPHERE_XMIN  { -1,  0,  0 }    // -X //
#define UNIT_SPHERE_YPLUS {  0,  1,  0 }    //  Y //
#define UNIT_SPHERE_YMIN  {  0, -1,  0 }    // -Y //
#define UNIT_SPHERE_ZPLUS {  0,  0,  1 }    //  Z //
#define UNIT_SPHERE_ZMIN  {  0,  0, -1 }    // -Z //

// for icosahedron
#define IH_CZ (0.89442719099991)   //  2/sqrt(5) //
#define IH_SZ (0.44721359549995)   //  1/sqrt(5) //

#define IH_C1 (0.951056516)        // cos(18),  //
#define IH_S1 (0.309016994)        // sin(18) //
#define IH_C2 (0.587785252)        // cos(54),  //
#define IH_S2 (0.809016994)        // sin(54) //
#define IH_X1 (IH_C1*IH_CZ)
#define IH_Y1 (IH_S1*IH_CZ)
#define IH_X2 (IH_C2*IH_CZ)
#define IH_Y2 (IH_S2*IH_CZ)

#define IH_Ip0     glm::vec3(   0.0,    0.0,  1.0  )
#define IH_Ip1     glm::vec3(-IH_X2, -IH_Y2,  IH_SZ)
#define IH_Ip2     glm::vec3( IH_X2, -IH_Y2,  IH_SZ)
#define IH_Ip3     glm::vec3( IH_X1,  IH_Y1,  IH_SZ)
#define IH_Ip4     glm::vec3(   0.0,  IH_CZ,  IH_SZ)
#define IH_Ip5     glm::vec3(-IH_X1,  IH_Y1,  IH_SZ)

#define IH_Im0     glm::vec3(-IH_X1, -IH_Y1, -IH_SZ)
#define IH_Im1     glm::vec3(   0.0, -IH_CZ, -IH_SZ)
#define IH_Im2     glm::vec3( IH_X1, -IH_Y1, -IH_SZ)
#define IH_Im3     glm::vec3( IH_X2,  IH_Y2, -IH_SZ)
#define IH_Im4     glm::vec3(-IH_X2,  IH_Y2, -IH_SZ)
#define IH_Im5     glm::vec3(   0.0,    0.0, -1.0  )

// vertices of a unit icosahedron
static const triangle icosahedron[20]=
{
    // front pole
    { {IH_Ip0, IH_Ip1, IH_Ip2}, },
    { {IH_Ip0, IH_Ip5, IH_Ip1}, },
    { {IH_Ip0, IH_Ip4, IH_Ip5}, },
    { {IH_Ip0, IH_Ip3, IH_Ip4}, },
    { {IH_Ip0, IH_Ip2, IH_Ip3}, },
    // mid
    { {IH_Ip1, IH_Im0, IH_Im1}, },
    { {IH_Im0, IH_Ip1, IH_Ip5}, },
    { {IH_Ip5, IH_Im4, IH_Im0}, },
    { {IH_Im4, IH_Ip5, IH_Ip4}, },
    { {IH_Ip4, IH_Im3, IH_Im4}, },
    { {IH_Im3, IH_Ip4, IH_Ip3}, },
    { {IH_Ip3, IH_Im2, IH_Im3}, },
    { {IH_Im2, IH_Ip3, IH_Ip2}, },
    { {IH_Ip2, IH_Im1, IH_Im2}, },
    { {IH_Im1, IH_Ip2, IH_Ip1}, },
    // back pole
    { {IH_Im3, IH_Im2, IH_Im5}, },
    { {IH_Im4, IH_Im3, IH_Im5}, },
    { {IH_Im0, IH_Im4, IH_Im5}, },
    { {IH_Im1, IH_Im0, IH_Im5}, },
    { {IH_Im2, IH_Im1, IH_Im5}, },
};

static const GLfloat one = 0.5f;

static const GLfloat cube_strip[] =
{
    -one, -one, -one, // 1
        -one, -one,  one, // 2
        -one,  one,  one, // 3
        one,  one,  one, // 4
        one,  one, -one, // 5
        -one,  one, -one, // 6
        -one, -one, -one, // 7
        one, -one, -one, // 8
        one, -one,  one, // 9
        -one, -one,  one, // 10
        -one,  one,  one, // 11
        -one,  one, -one, // 12
        one,  one, -one, // 13
        one, -one, -one, // 14
        one, -one,  one, // 15
        one,  one,  one  // 16
    };

#define CUBE_SIDES 16

static const glm::vec3 cube_strip2[] =
{
    glm::vec3(-one, -one, -one), // 1
    glm::vec3(-one, -one,  one), // 2
    glm::vec3(-one,  one,  one), // 3
    glm::vec3( one,  one,  one), // 4
    glm::vec3( one,  one, -one), // 5
    glm::vec3(-one,  one, -one), // 6
    glm::vec3(-one, -one, -one), // 7
    glm::vec3( one, -one, -one), // 8
    glm::vec3( one, -one,  one), // 9
    glm::vec3(-one, -one,  one), // 10
    glm::vec3(-one,  one,  one), // 11
    glm::vec3(-one,  one, -one), // 12
    glm::vec3( one,  one, -one), // 13
    glm::vec3( one, -one, -one), // 14
    glm::vec3( one, -one,  one), // 15
    glm::vec3( one,  one,  one)  // 16
};

/// Holds all state information relevant to a character as loaded using FreeType
struct Character
{
    GLuint TextureID;   // ID handle of the glyph texture
    glm::ivec2 Size;    // Size of glyph
    glm::ivec2 Bearing; // Offset from baseline to left/top of glyph
    GLuint Advance;     // Horizontal offset to advance to next glyph
};

class CDisplay : public CModel
{
public:
    CXyz xyz;
    //CModel data;
    CTrack track;
    CPalette pick;
    //
    bool is_atom_selected;
    bool is_picking;
    bool is_visible;
    bool is_show_bond;
    bool is_show_box;
    bool is_show_label;
    bool is_show_symbol;
    bool is_show_index;
    //
    int input_format;
    //
    uint selected_atom;
    uint selected_zatom;
    std::string selected_symbol;
    glm::vec3 selected_xyz;
    //
    GLuint VAOM, VBOM;
    GLuint VAOB, VBOB;
    GLuint VAOC, VBOC;
    GLuint VAOA, VBOA;
    //
    GLuint VAOT, VBOT;
    GLuint VAOF, VBOF;
    //
    GLfloat atom_scale;
    GLfloat bond_scale;
    GLfloat view_zoom;
    GLfloat font_scale;
    GLfloat target_width;
    GLfloat target_height;
    //
    std::vector<glm::vec3> v_cylinder;
    std::vector<glm::vec3> v_arrow;
    std::vector<glm::vec3> v_cone;
    std::vector<glm::vec3> v_sphere;
    std::vector<glm::vec3> v_supercell;
    std::map<GLchar, Character> Characters;
    //
    GLint nvtx;
    GLint cylinder_vertex;
    GLint arrow_vertex;
    //GLint acone_vertex;
    GLfloat box_side;
    GLfloat fix_side;
    glm::vec4 CColor;
    glm::vec3 initial_axis_position;
    glm::vec3 axis_position;
    glm::vec2 view_offset;
    //
    glm::mat4 Projection;
    glm::mat4 FixView;
    glm::mat4 mvp;
    glm::mat3 Rot;

    CDisplay(void);

    CShader AtomShader;
    CShader BondShader;
    CShader FontShader;
    CShader PickShader;
    CShader ArrowShader;
    //
    bool IsVisible(void){
      return is_visible;
    }
    //
    void ReadFile(std::string d,std::string f);
    void SaveFile(std::string f);
    //
    void CreateCube(void);
    void CreateCylinder(uint res);
    void CreateArrow(uint res);
    void CreateSphere(uint res);
    //
    void DeleteCube(void);
    void DeleteCylinder(void);
    void DeleteArrow(void);
    void DeleteSphere(void);
    void DeleteAllResources(void);
    //
    void SetCylinderBuffer(void);
    void SetArrowBuffer(void);
    void SetSphereBuffer(void);
    void SetViewOffset(void)
    {
        view_offset=glm::vec2(0.0f,0.0f);
    };
    //
    void DrawBox(GLuint Program);
    void DrawScene(int w, int h);
    void DrawAtoms(GLuint Program,bool b);
    void DrawBonds(GLuint Program);
    //
    void ShowBonds(bool b);
    void ShowAxes(GLuint Program);
    void ShowSymbols(bool b);
    void ShowIndex(bool b);
    //
    void DeleteAtom(uint u);
    // Deprecated
    //void SetAtomCoordinates(uint u,glm::vec3& v3);
    void UpdateAtomType(uint u);
    //
    void SetAtomScale(bool b);
    void SetBondScale(float scale);
    void SetZoom(float scale);
    void SetFont(float scale);
    //
    float GetFontSize(void)
    {
        return font_scale;
    };
    //
    // Track
    void SetTrackLastPosition(float x, float y);
    void SetTrackCurretPosition(float x, float y);
    void SetTrackRotation(float x, float y);
    void SetTrackLastRotation(void);
    //
    uint GetNumberOfAtoms(void)
    {
        return GetTotalAtoms();
    };
    /*
    bool IsModelUnChanged(void)
    {
        //return is_unchanged;
        return IsUpdated();
    };*/
    // Picking
    int pcolor[2];
    void SetClearColor(float x,float y,float z,float w)
    {
        CColor.x = x;
        CColor.y = y;
        CColor.z = z;
        CColor.w = w;
    };
    void SetAxesPosition(glm::vec3& v1)
    {
        axis_position = -v1;
    };
    void ResetScene(void);
    void ResetAxes(void);
    void ResetZoom(void);
    //
    void SetPicking(int cx, int cy);
    void ProcessPicking(bool b);
    //
    void LoadFont(void);
    void RenderText(void);
    void RenderText(GLuint Program, float s, std::string text);
    inline void LinearMix(glm::vec3 &a, glm::vec3 &b, GLfloat f, glm::vec3 &r);
    //void CenterCoordinates(void);
};

#endif

// END SPHERE
