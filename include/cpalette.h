/*
 * CPalette.h
 *
 * Copyright 2018 Edmanuel <eetorres@gmail.com>
 *
 * MIT License
 *
 * Copyright (c) 2018 Edmanuel Torres
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 */

#ifndef _CPALETTE_H_
#define _CPALETTE_H_

#include <iostream>
#include <glm/glm.hpp>
#include <vector>

#if defined(_WIN32)
typedef unsigned int uint;
#endif // defined

//typedef struct {
//  double r,g,b;
//} gm_rgb;

//typedef struct {
//  unsigned int r,g,b;
//} ui_rgb;

class CPalette
{

public:
    //
    unsigned int xcells, ycells, _lvls, _ipalette;
    double xdelta1, ydelta1, xdelta2, ydelta2;
    double y0, y1, x0, x1, x2, x3, x4, x5;
    double w0, dz, zmin, zmax;
    glm::vec3 rgb, cf1, cf2;
    glm::vec3 id_rgb;

    std::vector<glm::tvec3<float>> _color_palette;
    std::vector<glm::tvec3<float>> _index_palette;
    //std::vector<glm::tvec3<uint>> m_color_palette;
    CPalette();
    ~CPalette() {};
    // set
    void set_color(unsigned int);
    // get
    glm::vec3 get_color(unsigned int);
    glm::vec3 get_index(unsigned int);
    unsigned int get_index_rgb(int r, int g, int b);
    //unsigned int get_index_rg(glm::tvec3<uint>);
    //glm::tvec3<uint> get_vcolor(double);
    //
    void initialize(double mn, double mx, unsigned int l)
    {
        zmin=mn;
        zmax=mx;
        _lvls=l;
    };

//private:
    //
    glm::vec3 index_palette_(double val);
    glm::vec3 linear_palette_(double val);
    glm::vec3 hsv_palette_(double val);
    glm::vec3 rgb_palette_(double val);
    glm::vec3 earth_palette_(double val);
    glm::vec3 terrain_palette_(double val);
    //
    glm::vec3 palette_color_(double val);
    double color_interpolation_(double,double,double);
    //
    // Color functions
    void update_palette_real(void);
    void update_palette_index(void);
    void set(double w);
};

#endif

////


