Get the latest build of XMol-org for Linux and Windows7 at:

http://xmol.org/

On Linux it requires the following libraries:

libfreetype, libglfw and libXinerama.

License

This code is in the public domain. It is licensed under the MIT License,
if you have lawyers who are unhappy with public domain, dont use it.
Any external code may include another license.

Let me know if you have any issue!

![Main XMol windows](img/xmol_main_window_1.png)

![Main XMol windows](img/xmol_main_window_2.png)